# bw6-761

A minimal implementation of the curve BW6-761 in Sage and Magma, from the paper
[ePrint 2020/351](https://eprint.iacr.org/2020/351), Youssef El Housni and
Aurore Guillevic.

See this [Github link](https://github.com/yelhousni/zexe/tree/youssef/BW6-761-Fq-ABLR-2ML-M) for the Rust implementation and this [Github link](https://github.com/EYBlockchain/zk-swap-libff/tree/ey/libff/algebra/curves/bw6_761) for the C++ implementation.
