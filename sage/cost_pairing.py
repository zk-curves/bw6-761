from sage.rings.integer_ring import ZZ
from sage.rings.rational_field import QQ
from sage.misc.functional import cyclotomic_polynomial
from pairing import bits_2naf

def karabina_cost_exp(u0, mk, mkd, skd, ikd):
    """ Karabina cost of compressed squaring
    https://www.ams.org/journals/mcom/2013-82-281/S0025-5718-2012-02625-1/
    mk = multiplication in Fpk
    mkd = multiplication in F_{p^{k/d}}
    skd = squaring in F_{p^{k/d}}
    ikd = inversion in F_{p^{k/d}}
    """
    bits_u0 = (abs(u0)).digits(2)
    len_u = len(bits_u0)
    hw_u = sum(bits_u0)
    bits_2naf_u0 = bits_2naf(abs(u0))
    len2naf_u = len(bits_2naf_u0)
    hw2naf_u = sum([1 for bi in bits_2naf_u0 if bi != 0])
    cost_exp_to_u = 4*(len_u-1)*mkd + (6*(hw_u - 1) -3)*mkd + (hw_u - 1)*mk + 3*(hw_u - 1)*skd + ikd
    cost_exp_to_u_2naf = 4*(len2naf_u-1)*mkd + (6*(hw2naf_u - 1) -3)*mkd + (hw2naf_u - 1)*mk + 3*(hw2naf_u - 1)*skd + ikd
    return min(cost_exp_to_u, cost_exp_to_u_2naf)

def cost_pairing_bls12(u0):
    # cost
    # a=0
    # AKLGL'11
    # there are three formulas in https://www.lirmm.fr/arith18/papers/Chung-Squaring.pdf for squaring in cubic extensions
    # S3 = m + 4*s; S3 = 2*m+3*s; S3 = 3*m+2*s
    u0 = ZZ(abs(u0))
    m = 1
    s = 1
    m2 = 3*m
    s2 = 2*m
    m6 = 6*m2 # multiplication in cubic extension
    #s6 = m2+4*s2 # =3*m+4*2*m = 11m squaring in cubic extension
    s6 = 2*m2+3*s2 # =2*3*m+3*2*m = 12m squaring in cubic extension
    m12 = 3*m6
    s12 = 2*m6
    f2 = 8*m
    f12 = 5*m2 # TODO
    i = 25*m
    i12 = 94*m+i # cf Guillevic Masson Thome DCC'20 but is it compatible with the tower?
    i2 = 4*m + i
    k = 12
    # AKLGL
    me=m2 ; se=s2; sk=s12 # e = k/d
    double_line_ate = 3*me+6*se+(k//3)*m
    add_line_ate    = 11*me+2*se+(k//3)*m
    # Costello Lange Naehrig
    # double_line_ate_cln = 3*me+5*se+(k//3)*m    ???
    double_line_ate_cln = 2*me+7*se+(k//3)*m
    add_line_ate_cln    = 10*me+2*se+(k//3)*m
    update1        = 13*me+sk
    update2        = 13*me
    
    hw_u = sum((abs(u0)).digits(2))
    bits_2naf_u0 = bits_2naf(abs(u0))
    len2naf_u = len(bits_2naf_u0)
    hw2naf_u = sum([1 for bi in bits_2naf_u0 if bi != 0])

    cost_ate = (u0.nbits()-1)*double_line_ate + (u0.nbits()-2)*update1 + (hw_u-1)*(add_line_ate+update2)
    cost_ate_2naf = (len(bits_2naf_u0)-1)*double_line_ate + (len(bits_2naf_u0)-2)*update1 + (hw2naf_u-1)*(add_line_ate+update2)
    cost_ate_cln = (u0.nbits()-1)*double_line_ate_cln + (u0.nbits()-2)*update1 + (hw_u-1)*(add_line_ate_cln+update2)
    cost_ate_cln_2naf = (len(bits_2naf_u0)-1)*double_line_ate_cln + (len(bits_2naf_u0)-2)*update1 + (hw2naf_u-1)*(add_line_ate_cln+update2)
    print("m12 = {}m s12 = {}m".format(m12,s12))
    print("m2 = {}m s2 = {}m".format(m2,s2))

    print("cost ate Miller       = {}m".format(cost_ate))
    print("cost ate Miller 2-naf = {}m".format(cost_ate_2naf))
    print("({0}-1)*(3*m{2}+6*s{2}+(k//3)*m{2}) + ({0}-2)*(13*m{2}+sk) + ({1}-1)*(11*m{2}+2*s{2}+(k//3)*m{2}+13*m{2})".format(u0.nbits(),hw_u,4))
    print("({0}-1)*(3*m{2}+6*s{2}+(k//3)*m{2}) + ({0}-2)*(13*m{2}+sk) + ({1}-1)*(11*m{2}+2*s{2}+(k//3)*m{2}+13*m{2})".format(len(bits_2naf_u0),hw2naf_u,4))
    print("cost ate Miller       = {}m (Costello Lange Naehrig)".format(cost_ate_cln))
    print("cost ate Miller 2-naf = {}m (Costello Lange Naehrig)".format(cost_ate_cln_2naf))
    print("({0}-1)*(2*m{2}+7*s{2}+(k//3)*m{2}) + ({0}-2)*(13*m{2}+sk) + ({1}-1)*(10*m{2}+2*s{2}+(k//3)*m{2}+13*m{2})".format(u0.nbits(),hw_u,4))
    print("({0}-1)*(2*m{2}+7*s{2}+(k//3)*m{2}) + ({0}-2)*(13*m{2}+sk) + ({1}-1)*(10*m{2}+2*s{2}+(k//3)*m{2}+13*m{2})".format(len(bits_2naf_u0),hw2naf_u,4))

    min_cost_miller = min(cost_ate, cost_ate_2naf, cost_ate_cln, cost_ate_cln_2naf)
    
    # final exponentiation
    # (p^12-1)/r = (p^12-1)/Phi_12(u) * Phi_12(u)/r = (p^6-1)*(p^2+1)*(p^4-p^2+1)/r
    QQx = QQ['x']; (x,) = QQx._first_ngens(1)
    assert ((x**12-1) // cyclotomic_polynomial(12)) == (x**6-1)*(x**2+1)
    px = (x-1)**2*(x**4 - x**2 + 1)/3 + x
    rx = x**4-x**2+1
    l3 = (x-1)**2
    l2 = l3*x
    l1 = l2*x-l3
    l0 = l1*x + 3
    exponent = (px**4-px**2+1)//rx
    assert l0+px*(l1+px*(l2+px*l3)) == 3*exponent
    # now cost of exponentiation
    # easy part: exponent = (px**6-1)*(px**2+1)
    # px**6 is a conjugation: costs only 6 negations
    # one inversion in Fp12, one mult
    #f2_12 = 8*m # from cost_pairing.py DCC'2020
    # Simon Masson PhD thesis section 4.4.2
    f2_12 = 5*m2
    easy_part = i12 + m12 + f2_12 + m12
    # hard part: exponent = (px**4-px**2+1)//rx
    # assume that cyclotomic squarings are implemented (PKC'10 Granger Scott)
    # cost_exp_to_x = 4*(len2naf_u-1)*m2 + (6*(hw2naf_u - 1) -3)*m2 + (hw2naf_u - 1)*m12 + 3*(hw2naf_u - 1)*s2 + i2
    cost_exp_to_x = karabina_cost_exp(u0, m12, m2, s2, i2)
    s12cyclo = 18*m
    hard_part = 5*cost_exp_to_x + 10*m12 + 3*f12 + 2*s12cyclo
    print("cost final exp easy: {}m".format(easy_part))
    print("cost final exp hard: {}m (with Karabina technique)".format(hard_part))
    print("cost final exp:      {}m".format(easy_part + hard_part))
    print("\ncost pairing (total) {}m".format(easy_part + hard_part + min_cost_miller))

def cost_pairing_bls12_377():
    u0 = ZZ(0x8508C00000000001)
    cost_pairing_bls12(u0)
def cost_pairing_bls12_379():
    u0 = ZZ(0x9b04000000000001)
    cost_pairing_bls12(u0)
def cost_pairing_bls12_381():
    u0 = ZZ(-(2**63+2**62+2**60+2**57+2**48+2**16))
    cost_pairing_bls12(u0)
def cost_pairing_bls12_383():
    u0 = ZZ(0x105a8000000000001)
    cost_pairing_bls12(u0)

def cost_pairing_bls12_440():
    u0 = ZZ(-(2**73+2**72+2**50+2**24))
    cost_pairing_bls12(u0)

def cost_pairing_bls12_442():
    u0 = ZZ(-(2**12-2**48-2**71+2**74))
    cost_pairing_bls12(u0)

def cost_pairing_bls12_446():
    u0 = ZZ(-(2**74+2**73+2**63+2**57+2**50+2**17+1))
    cost_pairing_bls12(u0)

def cost_pairing_bls24(u0):
    # cost
    # a=0
    # AKLGL'11
    # there are three formulas in https://www.lirmm.fr/arith18/papers/Chung-Squaring.pdf for squaring in cubic extensions
    # S3 = m + 4*s; S3 = 2*m+3*s; S3 = 3*m+2*s
    u0 = ZZ(abs(u0))
    m = 1
    s = 1
    m2 = 3*m
    s2 = 2*m
    m4 = 3*m2
    s4 = 2*m2
    m12 = 6*m4 # multiplication in cubic extension
    #s12 = 1*m4+4*s4 # squaring in cubic extension
    s12 = 2*m4+3*s4 # squaring in cubic extension
    m24 = 3*m12
    s24 = 2*m12
    s24cyclo = 18*m2 # = 6*m4
    f24 = 22*m # 24 - 2 TODO
    i = 25*m
    i2 = 4*m + i
    i4 = 4*m2 + i2
    i12 = 12*m4 + i4 # 108m + i4
    i24 = 4*m12 + i12 # 216m + ...
    k = 24
    # AKLGL
    me=m4 ; se=s4; sk=s24 # e = k/d
    double_line_ate = 3*me+6*se+(k//3)*m
    add_line_ate    = 11*me+2*se+(k//3)*m
    # Costello Lange Naehrig
    # double_line_ate_cln = 3*me+5*se+(k//3)*m    ???
    double_line_ate_cln = 2*me+7*se+(k//3)*m
    add_line_ate_cln    = 10*me+2*se+(k//3)*m
    update1        = 13*me+sk
    update2        = 13*me
    
    hw_u = sum((abs(u0)).digits(2))
    bits_2naf_u0 = bits_2naf(abs(u0))
    hw2naf_u = sum([1 for bi in bits_2naf_u0 if bi != 0])

    cost_ate = (u0.nbits()-1)*double_line_ate + (u0.nbits()-2)*update1 + (hw_u-1)*(add_line_ate+update2)
    cost_ate_2naf = (len(bits_2naf_u0)-1)*double_line_ate + (len(bits_2naf_u0)-2)*update1 + (hw2naf_u-1)*(add_line_ate+update2)
    cost_ate_cln = (u0.nbits()-1)*double_line_ate_cln + (u0.nbits()-2)*update1 + (hw_u-1)*(add_line_ate_cln+update2)
    cost_ate_cln_2naf = (len(bits_2naf_u0)-1)*double_line_ate_cln + (len(bits_2naf_u0)-2)*update1 + (hw2naf_u-1)*(add_line_ate_cln+update2)
    print("m24 = {}m s24 = {}m".format(m24,s24))
    print("m4 = {}m s4 = {}m".format(m4,s4))
    print("cost ate Miller       = {}m".format(cost_ate))
    print("cost ate Miller 2-naf = {}m".format(cost_ate_2naf))
    print("({0}-1)*(3*m{2}+6*s{2}+(k//3)*m{2}) + ({0}-2)*(13*m{2}+sk) + ({1}-1)*(11*m{2}+2*s{2}+(k//3)*m{2}+13*m{2})".format(u0.nbits(),hw_u,4))
    print("({0}-1)*(3*m{2}+6*s{2}+(k//3)*m{2}) + ({0}-2)*(13*m{2}+sk) + ({1}-1)*(11*m{2}+2*s{2}+(k//3)*m{2}+13*m{2})".format(len(bits_2naf_u0),hw2naf_u,4))
    print("cost ate Miller       = {}m (Costello Lange Naehrig)".format(cost_ate_cln))
    print("cost ate Miller 2-naf = {}m (Costello Lange Naehrig)".format(cost_ate_cln_2naf))
    print("({0}-1)*(2*m{2}+7*s{2}+(k//3)*m{2}) + ({0}-2)*(13*m{2}+sk) + ({1}-1)*(10*m{2}+2*s{2}+(k//3)*m{2}+13*m{2})".format(u0.nbits(),hw_u,4))
    print("({0}-1)*(2*m{2}+7*s{2}+(k//3)*m{2}) + ({0}-2)*(13*m{2}+sk) + ({1}-1)*(10*m{2}+2*s{2}+(k//3)*m{2}+13*m{2})".format(len(bits_2naf_u0),hw2naf_u,4))

    min_cost_miller = min(cost_ate, cost_ate_2naf, cost_ate_cln, cost_ate_cln_2naf)
    # final exponentiation
    # (p^24-1)/r = (p^24-1)/Phi_24(u) * Phi_24(u)/r = (p^12-1)*(p^4+1)*(p^8-p^4+1)/r
    QQx = QQ['x']; (x,) = QQx._first_ngens(1)
    assert ((x**24-1) // cyclotomic_polynomial(24)) == (x**12-1)*(x**4+1)
    px = (x-1)**2*(x**8 - x**4 + 1)/3 + x
    rx = x**8-x**4+1
    l7 = (x-1)**2 # x**2-2*x+1 # this is (x-1)**2
    l6 = l7*x
    l5 = l6*x
    l4 = l5*x
    l3 = l4*x-l7
    l2 = l3*x
    l1 = l2*x
    l0 = l1*x + 3
    exponent = (px**8-px**4+1)//rx
    assert l0+px*(l1+px*(l2+px*(l3+px*(l4+px*(l5+px*(l6+px*l7)))))) == 3*exponent
    assert (l7*(x*x*x*x-1)*x*x*x+3)+px*(l7*(x*x*x*x-1)*x*x+px*(l7*(x*x*x*x-1)*x+px*(l7*(x*x*x*x-1)+px*(l7*x*x*x+px*(l7*x*x+px*(l7*x+px*l7)))))) == 3*exponent    
    # cost from GF08:
    # 8 exponentiations by u, 1 exponentiation by u/2, 1 squaring, 10 multiplications, 7 Frobenius in Fp24.
    # easy part: exponent = (px**12-1)*(px**4+1)
    # f^p12 is free (conjugation)
    # 1 Frobenius, 1 inversion, 2 multiplications
    f4_24 = (24//4 - 2) * 4 # 16m
    easy_part = f4_24 + i24 + 2*m24
    # hard part
    # exponent = (x-1)
    cost_exp_to_x_1 = karabina_cost_exp(u0-1, m24, m4, s4, i4)
    cost_exp_to_x = karabina_cost_exp(u0, m24, m4, s4, i4)
    hard_part = (2*cost_exp_to_x_1) + (cost_exp_to_x + m24 + f24)*7 + m24 + (s24cyclo + 2*m24)
    # total 18732 M + 10 I with Karabina compressed squarings
    # 23400 M + I with cyclotomic squaring of Granger-Scott.
    print("cost final exp easy: {}m".format(easy_part))
    print("cost final exp hard: {}m (with Karabina technique)".format(hard_part))
    print("cost final exp:      {}m".format(easy_part + hard_part))
    print("\ncost pairing (total) {}m".format(easy_part + hard_part + min_cost_miller))

def cost_pairing_bls24_318():
    u0 = ZZ(-2**32 + 2**28 + 2**12)
    cost_pairing_bls24(u0)
def cost_pairing_bls24_318b():
    u0 = ZZ(-2**32 + 2**28 - 2**23 + 2**21 + 2**18 + 2**12 - 1)
    cost_pairing_bls24(u0)
def cost_pairing_bls24_315():
    u0 = ZZ(-2**32 + 2**30 + 2**22 - 2**20 + 1)
    cost_pairing_bls24(u0)

def cost_pairing_kss16(u0):
    # cost
    # b=0
    u0 = ZZ(u0)
    m = 1
    s = 1
    m2 = 3*m
    s2 = 2*m
    m4 = 3*m2   # = 9m
    s4 = 2*m2   # = 6m
    m8 = 3*m4   # = 27m
    s8 = 2*m4   # = 18m
    m16 = 3*m8  # = 81m
    s16 = 2*m8  # = 54m
    # s16_cyclo = 4*m4 # m4=9*m
    # Granger Scott, Faster Squaring in the Cyclotomic Subgroup of Sixth Degree Extensions,
    # PKC 2010, pages 209-223.
    # http://www.iacr.org/archive/pkc2010/60560212/60560212.pdf
    s16_cyclo = 2*s8 # = 4*m4 indeed
    f16 = 15*m # Ghammam PhD thesis Table 4.12 p. 113
    c16_cyclo = s16_cyclo + m16 #8*m4 # cube, to check: s16_cyclo + m16 = 4*m4 + 9*m4, so where is 8*m4 it from?
    # INDOCRYPT 2012, Analysis of Optimum Pairing Products at High Security Levels
    # Xusheng Zhang and Dongdai Lin, but no formula provided
    i = 25*m
    i16 = 134*m+i # cf Guillevic Masson Thome DCC'20
    k = 16
    # AKLGL
    me=m4 ; se=s4; sk=s16 # e = k/d
    # Costello Lange Naehrig, with d=4, b=0
    double_line_ate_cln = 2*me+8*se+(k//2)*m
    add_line_ate_cln    = 9*me+5*se+(k//2)*m
    update1        = 8*me+sk
    update2        = 8*me    # in other words, a sparse multiplication costs 8 m4 (8, not 7)

    hw_u = sum((abs(u0)).digits(2))
    bits_2naf_u0 = bits_2naf(abs(u0))
    hw2naf_u = sum([1 for bi in bits_2naf_u0 if bi != 0])
    # cost_ate = (u0.nbits()-1)*double_line_ate + (u0.nbits()-2)*update1 + (hw2naf_u-1)*(add_line_ate+update2)
    # Frobenius(Q) : 2 sparse Frobenius in Fp16 (actually cheaper than 2*f16)
    additional_terms = 2*f16 + add_line_ate_cln + double_line_ate_cln + 2*m16 + f16
    cost_opt_ate_cln = (u0.nbits()-1)*double_line_ate_cln + (u0.nbits()-2)*update1 + (hw_u-1)*(add_line_ate_cln+update2) + additional_terms
    cost_opt_ate_cln_2naf = (len(bits_2naf_u0)-1)*double_line_ate_cln + (len(bits_2naf_u0)-2)*update1 + (hw2naf_u-1)*(add_line_ate_cln+update2) + additional_terms
    min_cost_miller = min(cost_opt_ate_cln, cost_opt_ate_cln_2naf)
    print("m16 = {}m s16 = {}m".format(m16,s16))
    print("m4 = {}m s4 = {}m".format(m4,s4))
    #print("cost ate Miller = {}m".format(cost_ate))
    print("cost opt ate Miller = {}m (Costello Lange Naehrig)".format(cost_opt_ate_cln))
    print("({0}-1)*(2*m{2}+8*s{2}+(k//2)*m{2}) + ({0}-2)*(8*m{2}+sk) + ({1}-1)*(9*m{2}+5*s{2}+(k//2)*m{2}+8*m{2})".format(u0.nbits(),hw_u,4))
    print("cost opt ate Miller = {}m (2-naf, Costello Lange Naehrig)".format(cost_opt_ate_cln_2naf))
    print("({0}-1)*(2*m{2}+8*s{2}+(k//2)*m{2}) + ({0}-2)*(8*m{2}+sk) + ({1}-1)*(9*m{2}+5*s{2}+(k//2)*m{2}+8*m{2})".format(len(bits_2naf_u0),hw2naf_u,4))

    # final exponentiation
    # (p^16-1)/r = (p^16-1)/Phi_16(u) * Phi_16(u)/r = (p^8-1)*(p^8+1)/r
    QQx = QQ['x']; (x,) = QQx._first_ngens(1)
    assert ((x**16-1) // cyclotomic_polynomial(16)) == (x**8-1)

    px = (x**10 + 2*x**9 + 5*x**8 + 48*x**6 + 152*x**5 + 240*x**4 + 625*x**2 + 2398*x + 3125)/980
    rx = (x**8 + 48*x**4 + 625)/61250 # 625 = 5^4, 61250 = 2*5^4*7^2
    tx = (2*x**5 + 41*x + 35)/35
    cx = 125 * (x**2 + 2*x + 5)/2 # C such that P+1-T = C*R
    yx = (x**5 + 5*x**4 + 38*x + 120)/70 # Y such that T^2 - 4*P = -4*Y^2
    betax = (x**9-11*x**8-16*x**7-120*x**6-32*x**5-498*x**4-748*x**3-2740*x**2-3115*x-5651)/4018
    lambx = (x**4 + 24)/7 # sqrt(-1) mod R
    k = 16
    D = 1
    exponent = (px**8+1)//rx
    # easy part: px**8 - 1
    easy_part = i16 + m16
    # hard part: (px**8+1) // rx
    # exponentiation to the power u
    exp_u = (u0.nbits()-1)*s16_cyclo + (hw_u-1)*m16
    # exponentiation to the power u+1
    hw_u1 = sum((abs(u0+1)).digits(2))
    exp_u1 = ((u0+1).nbits()-1)*s16_cyclo + (hw_u1-1)*m16
    hard_part = 7*exp_u + 2*exp_u1 + 34*s16_cyclo + 32*m16 + 7*f16 + 3*c16_cyclo

    exp_u_2naf = (len(bits_2naf_u0)-1)*s16_cyclo + (hw2naf_u-1)*m16
    bits_2naf_u01 = bits_2naf(abs(u0+1))
    hw2naf_u1 = sum([1 for bi in bits_2naf_u01 if bi != 0])
    exp_u1_2naf = (len(bits_2naf_u01)-1)*s16_cyclo + (hw2naf_u1-1)*m16
    hard_part_2naf = 7*exp_u_2naf + 2*exp_u1_2naf + 34*s16_cyclo + 32*m16 + 7*f16 + 3*c16_cyclo

    print("cost final exp easy:  {}m".format(easy_part))
    print("cost final exp hard:  {}m (with cyclotomic squarings)".format(hard_part))
    print("cost final exp hard:  {}m (2-naf with cyclotomic squarings)".format(hard_part_2naf))
    print("cost final exp:       {}m".format(easy_part + hard_part))
    print("cost final exp 2-naf: {}m".format(easy_part + hard_part_2naf))
    print("\ncost pairing (total)  {}m".format(easy_part + min(hard_part, hard_part_2naf) + min_cost_miller))

    print("new formula")
    # new formula     # total cost 33 S + 31 M + 2 exp(u+1) + 7 exp(u) + 14*f16 + 8 inv_frob_8
    hard_part = 7*exp_u + 2*exp_u1 + 33*s16_cyclo + 31*m16 + 14*f16
    hard_part_2naf = 7*exp_u_2naf + 2*exp_u1_2naf + 33*s16_cyclo + 31*m16 + 14*f16
    print("cost final exp hard:  {}m (with cyclotomic squarings)".format(hard_part))
    print("cost final exp hard:  {}m (2-naf with cyclotomic squarings)".format(hard_part_2naf))
    print("cost final exp:       {}m".format(easy_part + hard_part))
    print("cost final exp 2-naf: {}m".format(easy_part + hard_part_2naf))
    print("\ncost pairing (total)  {}m".format(easy_part + min(hard_part, hard_part_2naf) + min_cost_miller))

def cost_pairing_kss16_330():
    u0 = ZZ(-2**34 + 2**27 - 2**23 + 2**20 - 2**11 + 1)
    cost_pairing_kss16(u0)

def cost_pairing_kss16_330b():
    u0 = ZZ(2**34 - 2**30 + 2**26 + 2**23 + 2**14 - 2**5 + 1)
    cost_pairing_kss16(u0)

def cost_pairing_kss16_339():
    u0 = ZZ(2**35 - 2**32 - 2**18 + 2**8 + 1)
    cost_pairing_kss16(u0)

def cost_pairing_kss18(u0):
    # extension: Fp - Fp3 - Fp9 - Fp18
    # a=0
    # AKLGL'11
    # there are three formulas in https://www.lirmm.fr/arith18/papers/Chung-Squaring.pdf for squaring in cubic extensions
    # S3 = m + 4*s; S3 = 2*m+3*s; S3 = 3*m+2*s
    u0 = ZZ(abs(u0))
    m = 1
    s = 1
    m3 = 6*m
    s3 = 2*m+3*s
    m9 = 6*m3
    s9 = 2*m3+3*s3
    m18 = 3*m9
    s18 = 2*m9
    s18_cyclo = 6*m3
    f18 = 16*m # to check
    i = 25*m
    i18 = i + 300*m # TODO
    i18_cyclo = 0 # Frobenius power p^9 is only conjugation
    k = 18

    # AKLGL
    # k=18, d=6, e=k/d = 3
    k = 18
    d = 6
    k_d = k//d
    me=m3 ; se=s3; sk=s18 # e = k/d
    double_line_ate = 3*me+6*se+(k//3)*m
    add_line_ate    = 11*me+2*se+(k//3)*m
    add_line_ate_with_z = 11*me+2*se+(k//3)*m + 5*me # +5*me seems the upper bound
    # Costello Lange Naehrig
    # double_line_ate_cln = 3*me+5*se+(k//3)*m    ???
    double_line_ate_cln = 2*me+7*se+(k//3)*m
    add_line_ate_cln    = 10*me+2*se+(k//3)*m
    add_line_ate_with_z_cln = 14*me+2*se+(k//3)*m
    update1        = 13*me+sk
    update2        = 13*me

    hw_u = sum((abs(u0)).digits(2))
    bits_2naf_u0 = bits_2naf(abs(u0))
    hw2naf_u = sum([1 for bi in bits_2naf_u0 if bi != 0])

    additional_terms = double_line_ate + add_line_ate + add_line_ate_with_z + 5*f18 + 3*m18
    additional_terms_cln = double_line_ate_cln + add_line_ate_cln + add_line_ate_with_z_cln + 5*f18 + 3*m18

    cost_ate = (u0.nbits()-1)*double_line_ate + (u0.nbits()-2)*update1 + (hw_u-1)*(add_line_ate+update2) + additional_terms
    cost_ate_2naf = (len(bits_2naf_u0)-1)*double_line_ate + (len(bits_2naf_u0)-2)*update1 + (hw2naf_u-1)*(add_line_ate+update2) + additional_terms
    cost_ate_cln = (u0.nbits()-1)*double_line_ate_cln + (u0.nbits()-2)*update1 + (hw_u-1)*(add_line_ate_cln+update2) + additional_terms_cln
    cost_ate_cln_2naf = (len(bits_2naf_u0)-1)*double_line_ate_cln + (len(bits_2naf_u0)-2)*update1 + (hw2naf_u-1)*(add_line_ate_cln+update2) + additional_terms_cln
    print("m18 = {}m s18 = {}m".format(m18,s18))
    print("m9 = {}m s9 = {}m".format(m9,s9))
    print("m3 = {}m s3 = {}m".format(m3,s3))
    print("cost ate Miller       = {}m".format(cost_ate))
    print("cost ate Miller 2-naf = {}m".format(cost_ate_2naf))

    print("({0}-1)*(3*m{2}+6*s{2}+(k//3)*m{2}) + ({0}-2)*(13*m{2}+sk) + ({1}-1)*(11*m{2}+2*s{2}+(k//3)*m{2}+13*m{2})".format(u0.nbits(), hw_u, k_d))
    print("({0}-1)*(3*m{2}+6*s{2}+(k//3)*m{2}) + ({0}-2)*(13*m{2}+sk) + ({1}-1)*(11*m{2}+2*s{2}+(k//3)*m{2}+13*m{2})".format(len(bits_2naf_u0), hw2naf_u, k_d))
    print("cost ate Miller       = {}m (Costello Lange Naehrig)".format(cost_ate_cln))
    print("cost ate Miller 2-naf = {}m (Costello Lange Naehrig)".format(cost_ate_cln_2naf))
    print("({0}-1)*(2*m{2}+7*s{2}+(k//3)*m{2}) + ({0}-2)*(13*m{2}+sk) + ({1}-1)*(10*m{2}+2*s{2}+(k//3)*m{2}+13*m{2})".format(u0.nbits(), hw_u, k_d))
    print("({0}-1)*(2*m{2}+7*s{2}+(k//3)*m{2}) + ({0}-2)*(13*m{2}+sk) + ({1}-1)*(10*m{2}+2*s{2}+(k//3)*m{2}+13*m{2})".format(len(bits_2naf_u0), hw2naf_u, k_d))

    min_cost_miller = min(cost_ate, cost_ate_2naf, cost_ate_cln, cost_ate_cln_2naf)

    # final exponentiation
    # (p^18-1)/r = (p^18-1)/Phi_18(u) * Phi_18(u)/r = (p^9-1)*(p^3 + 1) * (p^6-p^3+1)/r
    QQx = QQ['x']; (x,) = QQx._first_ngens(1)
    assert ((x**18-1) // cyclotomic_polynomial(18)) == (x**9-1)*(x**3+1)
    px = (x**8 + 5*x**7 + 7*x**6 + 37*x**5 + 188*x**4 + 259*x**3 + 343*x**2 + 1763*x + 2401)/21
    rx = (x**6 + 37*x**3 + 343)/343 # 343 = 7^3
    # Algorithm 7.6 in Guide to Pairing-based cryptography

    # easy part: (px**9 - 1)*(p**3+1)
    easy_part = 2*f18 + i18 + 2*m18
    # exponentiation to the power u
    exp_u = (u0.nbits()-1)*s18_cyclo + (hw_u-1)*m18
    hard_part = 7*exp_u + 6*s18_cyclo + 53*m18 + 29*f18 + 8 * i18_cyclo

    exp_u_2naf = (len(bits_2naf_u0)-1)*s18_cyclo + (hw2naf_u-1)*m18
    hard_part_2naf = 7*exp_u_2naf + 6*s18_cyclo + 53*m18 + 35*f18

    new_hard_part = 7*exp_u + 19*s18_cyclo + 26*m18 + 10*f18 + 6 * i18_cyclo
    new_hard_part_2naf = 7*exp_u_2naf + 19*s18_cyclo + 26*m18 + 10*f18 + 6 * i18_cyclo
    #Cost: 7 exp(u) + 19 S + 26 M + 10 Frobenius powers + 6 Inversions with Frobenius power p^9

    print("cost final exp easy:  {}m".format(easy_part))
    print("cost final exp hard:  {}m (with cyclotomic squarings)".format(hard_part))
    print("cost final exp hard:  {}m (2-naf with cyclotomic squarings)".format(hard_part_2naf))
    print("cost final exp:       {}m".format(easy_part + hard_part))
    print("cost final exp 2-naf: {}m".format(easy_part + hard_part_2naf))
    print("\ncost pairing (total)  {}m".format(easy_part + min(hard_part, hard_part_2naf) + min_cost_miller))

    print("new formula:")
    print("cost final exp hard:  {}m (with cyclotomic squarings)".format(new_hard_part))
    print("cost final exp hard:  {}m (2-naf with cyclotomic squarings)".format(new_hard_part_2naf))
    print("cost final exp:       {}m".format(easy_part + new_hard_part))
    print("cost final exp 2-naf: {}m".format(easy_part + new_hard_part_2naf))
    print("\ncost pairing (total)  {}m".format(easy_part + min(new_hard_part, new_hard_part_2naf) + min_cost_miller))
