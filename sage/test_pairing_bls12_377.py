from sage.all_cmdline import *   # import sage library

from sage.rings.integer_ring import ZZ
from sage.rings.rational_field import QQ
from sage.misc.functional import cyclotomic_polynomial
from sage.rings.finite_rings.finite_field_constructor import FiniteField, GF
from sage.schemes.elliptic_curves.constructor import EllipticCurve

# this is much much faster with this statement:
# proof.arithmetic(False)
from sage.structure.proof.all import arithmetic

from pairing import *
from pairing_weil_bls import *
from test_pairing import *
from test_pairing_weil_bls import test_bilinearity_beta_weil_bls_even
from cost_pairing import cost_pairing_bls12

from test_scalar_mult import test_glv_scalar_mult_g1

if __name__ == "__main__":
    arithmetic(False)
    #preparse("QQx.<x> = QQ[]")
    QQx = QQ['x']; (x,) = QQx._first_ngens(1)
    # BLS12-377 seed
    u0 = ZZ(0x8508C00000000001)
    # BLS12 polynomials
    px = (x**6 - 2*x**5 + 2*x**3 + x + 1)/3
    rx = x**4-x**2+1
    tx = x+1
    cx = (x-1)**2/3
    yx = (x-1)*(2*x**2 - 1)/3
    betax = x**5 - 3*x**4 + 3*x**3 - x + 1
    lambx = x**2 -1
    c2x = (x**8 - 4*x**7 + 5*x**6 - 4*x**4 + 6*x**3 - 4*x**2 - 4*x + 13)/9 # cofactor for G2

    cost_pairing_bls12(u0)
    
    # check formula for final exponentiation
    assert ((x**12-1) // cyclotomic_polynomial(12)) == (x**6-1)*(x**2+1)
    exponent = (px**4-px**2+1)//rx
    l3 = (x-1)**2
    l2 = l3*x
    l1 = l2*x-l3
    l0 = l1*x+3
    assert l0+px*(l1+px*(l2+px*l3)) == 3*exponent
    exponent_easy = (px**6-1)*(px**2+1)
    exponent_hard = 3*(px**4-px**2+1)//rx
    print("exponent_easy = (px**6-1)*(px**2+1)")
    print("exponent_hard = 3*(px**4-px**2+1)//rx = {}".format(exponent_hard))

    p = ZZ(px(u0))
    r = ZZ(rx(u0))
    c = ZZ(cx(u0))
    c2 = ZZ(c2x(u0))
    t = ZZ(tx(u0))
    y = ZZ(yx(u0))
    Fp = GF(p, proof=False)
    E = EllipticCurve([Fp(0), Fp(1)])
    lambda_mod_r = ZZ(lambx(u0))
    beta_mod_p = Fp(betax(u0))
    # sextic twist over GF(p^2)
    #Fpz.<z> = Fp[]
    #Fp2.<i> = Fp.extension(z**2-5)
    #Fp2s.<s> = Fp2[]
    #Fp12.<j> = Fp2.extension(s**6 - i)
    assert p-1 == (2**46 * 3 * 7 * 13 * 499) * 53 * 409 * 2557 * 6633514200929891813 * 73387170334035996766247648424745786170238574695861388454532790956181
    print("p-1 = (u-1)*(u^5-u^4-u^3+u^2+u+2)/3\n    = (2**46 * 3 * 7 * 13 * 499) * (53 * 409 * 2557 * 6633514200929891813 * 73387170334035996766247648424745786170238574695861388454532790956181)")
    # rx-1 == (x - 1) * (x + 1) * x^2
    assert r-1 == (2**46 * 3 * 7 * 13 * 499) * (2 * 5 * 958612291309063373) * 9586122913090633729**2
    print("r-1 = (u-1)*(u+1)*u^2\n    = (2**46 * 3 * 7 * 13 * 499) * (2 * 5 * 958612291309063373) * 9586122913090633729**2")

    # co-factor of GT
    cofactorGT = (p**4 - p**2 + 1)//r
    cofactorGT == 73 * 733 * 9907133392668042525879507032505736467897843055444091915449145127341979335245297039623613430249313493610313967651813052567591541348837126946242723119127718146453611530031520691590326560874282582459918946440322689054712022836308086271423008860459900177148220797027656526576202902477337430474960665692030443666345467155806114621584830666273889800768834356323344510407388976077

    Fpz = Fp['z']; (z,) = Fpz._first_ngens(1)
    Fp2 = Fp.extension(z**2 -5 , names=('i',)); (i,) = Fp2._first_ngens(1)
    Fp2s = Fp2['s']; (s,) = Fp2s._first_ngens(1)
    #Fp12M = Fp2.extension(s**6 - (i+5), names=('wM',)); (wM,) = Fp12M._first_ngens(1)
    #EM = EllipticCurve([Fp2(0), Fp2(i+5)])
    #E12M = EllipticCurve([Fp12M(0), Fp12M(1)]) # but un Sage, an elliptic curve over an extension field in two layers is not handled easily
    # note: s^6 = i+5 <=> s^6-5 = i => s^12 -10*s^6 + 25 = i^2 = 5 so s^12 - 10*s^6 + 20 = 0
    #Fp12M_A = Fp.extension(z**12 - 10*z**6 + 20, names=('SM',)); (SM,) = Fp12M_A._first_ngens(1)
    #E12M_A = EllipticCurve([Fp12M_A(0), Fp12M_A(1)])

    Fp12M = Fp2.extension(s**6 - 2*i, names=('wM',)); (wM,) = Fp12M._first_ngens(1)
    Fq6M = Fp12M
    xiM = 2*i
    EM = EllipticCurve([Fp2(0), Fp2(2*i)])
    E12M = EllipticCurve([Fp12M(0), Fp12M(1)])
    # note: s^6 = 2*i => s^12 = 4*i^2 = 20 so s^12 - 20 = 0
    Fp12M_A = Fp.extension(z**12 - 20, names=('SM',)); (SM,) = Fp12M_A._first_ngens(1)
    E12M_A = EllipticCurve([Fp12M_A(0), Fp12M_A(1)])

    try:
        test_xiM = -Fp12M.modulus().constant_coefficient()
        print("xiM == -Fp12M.modulus().constant_coefficient(): {}".format(xiM == test_xiM))
    except AttributeError as err:
        print("xiM = -Fp12M.modulus().constant_coefficient() raised an error:\n{}".format(err))
    try:
        test_xiM = -Fp12M.polynomial().constant_coefficient() # works only for absolute extensions on prime fields
        print("xiM == -Fp12M.polynomial().constant_coefficient(): {}".format(xiM == test_xiM))
    except AttributeError as err:
        print("xiM = -Fp12M.polynomial().constant_coefficient() raised an error:\n{}".format(err))

    def map_Fp12M_Fp12M_A(x):
        # evaluate elements of Fp12M = Fp[i]/(i^2-5)[s]/(s^6-2*i) at i=S^6/2 and s=S
        # i <-> s^6/2 = SM^6/2 and s <-> SM
        #return sum([sum([yj*(SM**6/2)**j for j,yj in enumerate(xi.polynomial())]) * SM**e for e,xi in enumerate(x.list())])
        return sum([xi.polynomial()(SM**6/2) * SM**e for e,xi in enumerate(x.list())])
    def map_Fq6M_Fp12M_A(x, aM):
        return sum([xi.polynomial()(aM**6/2) * aM**e for e,xi in enumerate(x.list())])
    def map_Fp2_Fp12M_A(x):
        # evaluate elements of Fq=Fp[i] at i=s^6/2 = S^6/2
        return x.polynomial()(SM**6/2)

    ED = EllipticCurve([Fp2(0), Fp2(1/i)]) # -1/i works too
    Fp12D = Fp2.extension(s**6 - i, names=('wD',)); (wD,) = Fp12D._first_ngens(1)
    Fq6D = Fp12D
    xiD = i
    E12D = EllipticCurve([Fp12D(0), Fp12D(1)])

    # note: s^6 = i => s^12 = i^2 = 5 so s^12 - 5 = 0
    Fp12D_A = Fp.extension(z**12 - 5, names=('SD',)); (SD,) = Fp12D_A._first_ngens(1)
    E12D_A = EllipticCurve([Fp12D_A(0), Fp12D_A(1)])

    try:
        test_xiD = -Fp12D.modulus().constant_coefficient()
        print("xiD == -Fp12D.modulus().constant_coefficient(): {}".format(xiD == test_xiD))
    except AttributeError as err:
        print("xiD = -Fp12D.modulus().constant_coefficient() raised an error:\n{}".format(err))
    try:
        test_xiD = -Fp12D.polynomial().constant_coefficient() # works only for absolute extensions on prime fields
        print("xiD == -Fp12D.polynomial().constant_coefficient(): {}".format(xiD == test_xiD))
    except AttributeError as err:
        print("xiD = -Fp12D.polynomial().constant_coefficient() raised an error:\n{}".format(err))

    def map_Fp12D_Fp12D_A(x):
        # evaluate elements of Fp12D = Fp[i]/(i^2-5)[s]/(s^6-i) at i=S^6 and s=S
        # i <-> wD^6 = SD^6 and wD <-> SD
        #return sum([sum([yj*(SD**6)**j for j,yj in enumerate(xi.polynomial())]) * SD**i for i,xi in enumerate(x.list())])
        return sum([xi.polynomial()(SD**6) * SD**e for e,xi in enumerate(x.list())])
    def map_Fq6D_Fp12D_A(x, aD):
        return sum([xi.polynomial()(aD**6) * aD**e for e,xi in enumerate(x.list())])

    def map_Fp2_Fp12D_A(x):
        # evaluate elements of Fq=Fp[i] at i=wD^6 = SD^6
        return x.polynomial()(SD**6)

    print("test E (G1)")
    test_order(E,r*c)
    print("test E' (G2) M-twist")
    test_order(EM,r*c2)

    print("test Frobenius map on G2 with M-twist")
    test_g2_frobenius_eigenvalue(E12M_A,EM,Fp12M,map_Fq6M_Fp12M_A,r,c2,D_twist=False)
    test_g2_frobenius_eigenvalue_alt(E12M_A,EM,map_Fp2_Fp12M_A,r,c2,D_twist=False)
    print("test Frobenius map on G2 with D-twist")
    test_g2_frobenius_eigenvalue(E12D_A,ED,Fp12D,map_Fq6D_Fp12D_A,r,c2,D_twist=True)
    test_g2_frobenius_eigenvalue_alt(E12D_A,ED,map_Fp2_Fp12D_A,r,c2,D_twist=True)

    print("test GLV on G1")
    test_glv_scalar_mult_g1(E, lambda_mod_r, beta_mod_p, r, c)
    
    print("test Miller M-twist")
    test_miller_function_tate(E, E12M, EM, r, c, c2, D_twist=False)
    test_miller_function_tate_2naf(E, E12M, EM, r, c, c2, D_twist=False)

    test_miller_function_ate(E,E12M,EM,r,c,c2,u0,D_twist=False)
    test_miller_function_ate_2naf(E,E12M,EM,r,c,c2,u0,D_twist=False)
    test_miller_function_ate_aklgl(E,EM,Fp12M,xiM,r,c,c2,u0,D_twist=False)
    test_miller_function_ate_2naf_aklgl(E,EM,Fp12M,xiM,r,c,c2,u0,D_twist=False)

    #P12 = E12(P)
    #Q12 = E12(Q2)
    #P12.ate_pairing(Q12, r, 12, t, q=p) # very very slow

    print("test E'' (G2) D-twist")
    test_order(ED,r*c2)
    print("test Miller D-twist")
    test_miller_function_tate(E, E12D, ED, r, c, c2, D_twist=True)
    test_miller_function_tate_2naf(E, E12D, ED, r, c, c2, D_twist=True)

    test_miller_function_ate(E,E12D,ED,r,c,c2,u0,D_twist=True)
    test_miller_function_ate_2naf(E,E12D,ED,r,c,c2,u0,D_twist=True)
    test_miller_function_ate_aklgl(E,ED,Fp12D,xiD,r,c,c2,u0,D_twist=True)
    test_miller_function_ate_2naf_aklgl(E,ED,Fp12D,xiD,r,c,c2,u0,D_twist=True)

    print("\nFinal exponentiation")
    ee = ((px**6-1)*(px**2+1)*3*(px**4-px**2+1)//rx)(u0)
    test_final_exp_easy_k12(Fp12D_A)
    test_final_exp_bls12(Fp12D_A,r,u0,expected_exponent=ee)
    test_final_exp_2naf_bls12(Fp12D_A,r,u0,expected_exponent=ee)

    test_final_exp_easy_k12(Fp12M_A)
    test_final_exp_bls12(Fp12M_A,r,u0,expected_exponent=ee)
    test_final_exp_2naf_bls12(Fp12M_A,r,u0,expected_exponent=ee)

    print("\npairing")
    test_ate_pairing_bls12_aklgl(E,EM,r,c,c2,u0,Fp12M,map_Fp12M_Fp12M_A,D_twist=False)
    test_ate_pairing_bls12_aklgl(E,ED,r,c,c2,u0,Fp12D,map_Fp12D_Fp12D_A,D_twist=True)

    test_ate_pairing_bls12_2naf_aklgl(E,EM,r,c,c2,u0,Fp12M,map_Fp12M_Fp12M_A,D_twist=False)
    test_ate_pairing_bls12_2naf_aklgl(E,ED,r,c,c2,u0,Fp12D,map_Fp12D_Fp12D_A,D_twist=True)

    test_bilinearity_beta_weil_bls_even(E, E12M_A, EM, Fq6M, r, c, c2, u0, r, map_Fq6M_Fp12M_A, D_twist=False, function_name=weil_bls12, final_exp_hard=final_exp_hard_bls12)
    test_bilinearity_beta_weil_bls_even(E, E12D_A, ED, Fq6D, r, c, c2, u0, r, map_Fq6D_Fp12D_A, D_twist=True, function_name=weil_bls12, final_exp_hard=final_exp_hard_bls12)

    test_bilinearity_beta_weil_bls_even(E, E12M_A, EM, Fq6M, r, c, c2, u0, u0, map_Fq6M_Fp12M_A, D_twist=False, function_name=alpha_weil_bls12, final_exp_hard=final_exp_hard_bls12)
    test_bilinearity_beta_weil_bls_even(E, E12D_A, ED, Fq6D, r, c, c2, u0, u0, map_Fq6D_Fp12D_A, D_twist=True, function_name=alpha_weil_bls12, final_exp_hard=final_exp_hard_bls12)

    test_bilinearity_beta_weil_bls_even(E, E12M_A, EM, Fq6M, r, c, c2, u0, u0, map_Fq6M_Fp12M_A, D_twist=False, function_name=beta_weil_bls12, final_exp_hard=final_exp_hard_bls12)
    test_bilinearity_beta_weil_bls_even(E, E12D_A, ED, Fq6D, r, c, c2, u0, u0, map_Fq6D_Fp12D_A, D_twist=True, function_name=beta_weil_bls12, final_exp_hard=final_exp_hard_bls12)

    test_bilinearity_beta_weil_bls_even(E, E12M_A, EM, Fq6M, r, c, c2, u0, u0, map_Fq6M_Fp12M_A, D_twist=False, function_name=beta_weil_bls12_kinoshita_suzuki, final_exp_hard=final_exp_hard_bls12)
    test_bilinearity_beta_weil_bls_even(E, E12D_A, ED, Fq6D, r, c, c2, u0, u0, map_Fq6D_Fp12D_A, D_twist=True, function_name=beta_weil_bls12_kinoshita_suzuki, final_exp_hard=final_exp_hard_bls12)
