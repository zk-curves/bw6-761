# Overview
This directory contains SageMath implementation of `BW6-761` curve for formulas verification.

# Directory structure

The high-level structure of the repository is as follows.

* `pairing.py` contains formulas code and
* `test_pairing_BW6_761.py` contains tests.

# Build guide

Install [SageMath](https://doc.sagemath.org/html/en/installation/).

## Test
```bash
sage test_pairing_BW6_761.py
```
