from pairing import *

# BLS-12

def weil_bls12(P, Q, b, r):
    """
    INPUT:
    - `P` on E(Fp)
    - `Q` on E(Fpk)
    - `b` curve coefficient
    - `r` prime subgroup order

    Computes (f_{r,Q}(P) / f_{r,P}(Q) )^(p^6-1)(p^2+1) (the sign -1 vanishes)
    """
    f_r_Q_P, uQ = miller_function_ate(Q, P, 0, r)
    f_r_P_Q, uP = miller_function_ate(P, Q, 0, r)
    #g = f_r_Q_P / f_r_P_Q
    g = f_r_Q_P * f_r_P_Q.frobenius(6)
    return final_exp_easy_k12(g)

def alpha_weil_bls12(P, Q, b, u):
    """
    INPUT:
    - `P` on E(Fp)
    - `Q` on E(Fpk)
    - `b` curve coefficient
    - `u` seed

    Computes (f_{u^2,Q}(P) / f_{u^2,P}(Q) )^(p^6-1)(p^2+1) (the sign -1 vanishes)
    """
    u2 = u**2
    f_u_Q_P, uQ = miller_function_ate(Q, P, 0, u2)
    f_u_P_Q, uP = miller_function_ate(P, Q, 0, u2)
    #g = f_u_Q_P / f_u_P_Q
    g = f_u_Q_P * f_u_P_Q.frobenius(6)
    return final_exp_easy_k12(g)

def beta_weil_bls12(P, Q, b, u):
    """
    INPUT:
    - `P` on E(Fp)
    - `Q` on E(Fpk)
    - `b` curve coefficient
    - `u` seed

    Equation (6) in PAIRING'2012
    Implementing Pairings at the 192-bit Security Level
    Diego F. Aranha, Laura Fuentes-Castaneda, Edward Knapp,
    Alfred Menezes, and Francisco Rodriguez-Henriquez

    Compute
    ((f_{u,P}(Q)/f_{u,Q}(P))^p * f_{u,pP}(Q)f_{u,Q}(pP))^(p^6-1)(p^2+1)
    """
    f_u_P_Q, uP = miller_function_ate(P, Q, 0, u)
    f_u_Q_P, uQ = miller_function_ate(Q, P, 0, u)

    pP = P.curve() (uP[0]/uP[3], uP[1]/(uP[2]*uP[3])) # p*P in Jacobian coordinates
    f_u_pP_Q, u2P = miller_function_ate(pP, Q, 0, u)
    f_u_Q_pP, uQ = miller_function_ate(Q, pP, 0, u)

    #g = (f_u_P_Q / f_u_Q_P).frobenius() * (f_u_pP_Q / f_u_Q_pP)
    g = (f_u_P_Q * f_u_Q_P.frobenius(6)).frobenius() * (f_u_pP_Q * f_u_Q_pP.frobenius(6))

    return final_exp_easy_k12(g)

def beta_weil_bls12_kinoshita_suzuki(P, Q, b, u):
    """
    INPUT:
    - `P` on E(Fp)
    - `Q` on E(Fpk)
    - `b` curve coefficient
    - `u` seed

    Equation page 16 in IWSEC'2020
    Accelerating beta Weil pairing with
    precomputation and multi-pairing techniques
    Kai Kinoshita and Koutarou Suzuki
    Toyohashi University of Technology, Aichi, Japan

    (f_{u,pQ}(P) f_{u,-P}(pQ) * f_{u,Q}(pP) f_{u,-pP}(Q))^(p^6-1)(p^2+1)
    """
    # compute p*Q with Frobenius
    pQ = Q.curve() (Q[0].frobenius(), Q[1].frobenius())
    #f_{u,pQ}(P)
    f_u_pQ_P, upQ = miller_function_ate(pQ, P, 0, u)
    # f_{u,-P}(pQ) will also compute -u*P
    f_u_P_pQ, uP = miller_function_ate(-P, pQ, 0, u)

    pP = P.curve() (uP[0]/uP[3], -uP[1]/(uP[2]*uP[3])) # p*P in Jacobian coordinates
    # f_{u,-pP}(Q)
    f_u_pP_Q, u2P = miller_function_ate(-pP, Q, 0, u)
    # f_{u,Q}(pP)
    f_u_Q_pP, uQ = miller_function_ate(Q, pP, 0, u)

    g = f_u_P_pQ * f_u_pQ_P * f_u_pP_Q * f_u_Q_pP

    gp6 = g.frobenius(6)
    inv_g = 1/g
    m = gp6*inv_g # g^(q^6-1)

    return m

# BLS-24

def weil_bls24(P, Q, b, r):
    """
    INPUT:
    - `P` on E(Fp)
    - `Q` on E(Fpk)
    - `b` curve coefficient
    - `r` prime subgroup order

    Computes (f_{r,Q}(P) / f_{r,P}(Q) )^(p^12-1)(p^4+1) (the sign -1 vanishes)
    """
    f_r_Q_P, uQ = miller_function_ate(Q, P, 0, r)
    f_r_P_Q, uP = miller_function_ate(P, Q, 0, r)
    g = f_r_Q_P * f_r_P_Q.frobenius(12)
    return final_exp_easy_k24(g)

def alpha_weil_bls24(P, Q, b, u):
    """
    INPUT:
    - `P` on E(Fp)
    - `Q` on E(Fpk)
    - `b` curve coefficient
    - `u` seed

    Computes (f_{u^4,Q}(P) / f_{u^4,P}(Q) )^(p^12-1)(p^4+1) (the sign -1 vanishes)
    """
    u4 = u**4
    f_u_Q_P, uQ = miller_function_ate(Q, P, 0, u4)
    f_u_P_Q, uP = miller_function_ate(P, Q, 0, u4)
    #g = f_u_Q_P / f_u_P_Q
    g = f_u_Q_P * f_u_P_Q.frobenius(12)
    return final_exp_easy_k24(g)

def beta_weil_bls_even_k(k, P, Q, b, u, final_exp_easy_k=None):
    """
    INPUT:
    - `k` even embedding degre (multiple of 6)
    - `P` on E(Fp)
    - `Q` on E(Fpk)
    - `b` curve coefficient
    - `u` seed

    Generalization of Equation (6) in PAIRING'2012
    Implementing Pairings at the 192-bit Security Level
    Diego F. Aranha, Laura Fuentes-Castaneda, Edward Knapp,
    Alfred Menezes, and Francisco Rodriguez-Henriquez

    Compute
    e = k//6
    (product_{i=0}^{e-1} (f_{u,Q}([p^i]P) / f_{u,[p^i]P}(Q))^{p^{e-1-i}} )^(p^{k/2}-1)
    """
    assert (k % 6) == 0
    inv_with_frobenius = final_exp_easy_k is not None
    e = k//6
    piP = P # i=0 at first
    g = 1
    for i in range(0, e):
        j = e-1-i
        #f_{u,Q}([p^i]P)
        f_u_Q_piP, uQ = miller_function_ate(Q, piP, 0, u)
        # f_{u,[p^i]P}(Q) will also compute u*[p^i]P
        f_u_piP_Q, upiP = miller_function_ate(piP, Q, 0, u)

        piP = P.curve() (upiP[0]/upiP[3], upiP[1]/(upiP[2]*upiP[3])) # p*P in Jacobian coordinates

        if inv_with_frobenius:
            f = f_u_piP_Q * f_u_Q_piP.frobenius(k//2)
        else:
            f = f_u_piP_Q / f_u_Q_piP
        if j > 0:
            g = g * f.frobenius(j)
        else:
            g = g * f

    if inv_with_frobenius:
        return final_exp_easy_k(g)
    # else:
    gpk2 = g.frobenius(k//2)
    inv_g = 1/g
    m = gpk2*inv_g # g^(q^{k//2}-1)
    return m

def beta_weil_bls24(P, Q, b, u):
    """
    INPUT:
    - `P` on E(Fp)
    - `Q` on E(Fpk)
    - `b` curve coefficient
    - `u` seed

    Generalization of Equation (6) in PAIRING'2012
    Implementing Pairings at the 192-bit Security Level
    Diego F. Aranha, Laura Fuentes-Castaneda, Edward Knapp,
    Alfred Menezes, and Francisco Rodriguez-Henriquez

    k = 24
    e = k//6 = 4
    (product_{i=0}^{e-1} (f_{u,Q}([p^i]P) / f_{u,[p^i]P}(Q))^{p^{e-1-i}} )^(p^{k/2}-1)
    """
    return beta_weil_bls_even_k(24, P, Q, b, u, final_exp_easy_k=final_exp_easy_k24)

def beta_weil_bls24_kinoshita_suzuki(P, Q, b, u):
    """
    INPUT:
    - `P` on E(Fp)
    - `Q` on E(Fpk)
    - `b` curve coefficient
    - `u` seed

    Equation page 16 in IWSEC'2020
    Accelerating beta Weil pairing with
    precomputation and multi-pairing techniques
    Kai Kinoshita and Koutarou Suzuki
    Toyohashi University of Technology, Aichi, Japan

    (f_{u,p3Q}(P) f_{u,-P}(p3Q) * f_{u,p2Q}(pP) f_{u,-pP}(p2Q)
    *f_{u,pQ}(p2P) f_{u,-p2P}(pQ) * f_{u,Q}(p3P) f_{u,-p3P}(Q))^(p^12-1)(p^4+1)
    """
    # compute p3*Q with Frobenius
    p3Q = Q.curve() (Q[0].frobenius(3), Q[1].frobenius(3))
    #f_{u,p3Q}(P)
    f_u_p3Q_P, up3Q = miller_function_ate(p3Q, P, 0, u)
    # f_{u,-P}(p3Q) will also compute -u*P
    f_u_P_p3Q, uP = miller_function_ate(-P, p3Q, 0, u)

    pP = P.curve() (uP[0]/uP[3], -uP[1]/(uP[2]*uP[3])) # p*P in Jacobian coordinates
    # compute p2*Q with Frobenius
    p2Q = Q.curve() (Q[0].frobenius(2), Q[1].frobenius(2))
    # f_{u,-pP}(p2Q)
    f_u_pP_p2Q, u2P = miller_function_ate(-pP, p2Q, 0, u)
    # f_{u,p2Q}(pP)
    f_u_p2Q_pP, up2Q = miller_function_ate(p2Q, pP, 0, u)

    p2P = P.curve() (u2P[0]/u2P[3], -u2P[1]/(u2P[2]*u2P[3])) # p*P in Jacobian coordinates
    # compute p*Q with Frobenius
    pQ = Q.curve() (Q[0].frobenius(), Q[1].frobenius())
    # f_{u,-p2P}(pQ)
    f_u_p2P_pQ, u3P = miller_function_ate(-p2P, pQ, 0, u)
    # f_{u,pQ}(p2P)
    f_u_pQ_p2P, upQ = miller_function_ate(pQ, p2P, 0, u)

    p3P = P.curve() (u3P[0]/u3P[3], -u3P[1]/(u3P[2]*u3P[3])) # p*P in Jacobian coordinates
    # f_{u,-p3P}(Q)
    f_u_p3P_Q, u4P = miller_function_ate(-p3P, Q, 0, u)
    # f_{u,Q}(p3P)
    f_u_Q_p3P, uQ = miller_function_ate(Q, p3P, 0, u)
    
    g = f_u_P_p3Q * f_u_p3Q_P * f_u_pP_p2Q * f_u_p2Q_pP * f_u_p2P_pQ * f_u_pQ_p2P * f_u_p3P_Q * f_u_Q_p3P

    gp12 = g.frobenius(12)
    inv_g = 1/g
    m = gp12*inv_g # g^(q^12-1)

    return m

def beta_weil_bls_even_k_kinoshita_suzuki(k, P, Q, b, u):
    """
    INPUT:
    - `k` even embedding degre (multiple of 6)
    - `P` on E(Fp)
    - `Q` on E(Fpk)
    - `b` curve coefficient
    - `u` seed

    Equation page 16 in IWSEC'2020
    Accelerating beta Weil pairing with
    precomputation and multi-pairing techniques
    Kai Kinoshita and Koutarou Suzuki
    Toyohashi University of Technology, Aichi, Japan

    e = k//6
    (product_{i=0}^{e-1} f_{u,pi_{p^{e-1-i}}(Q)}([p^i]P) *
    f_{u,-[p^i]P}(pi_{p^{e-1-i}}(Q)) )^(p^{k/2}-1)
    """
    assert (k % 6) == 0
    e = k//6
    piP = P # i=0 at first
    g = 1
    for i in range(0, e):
        # compute \pi_{p^{e-1-i}}(Q) with Frobenius
        j = e-1-i
        if j > 0:
            pjQ = Q.curve() (Q[0].frobenius(j), Q[1].frobenius(j))
        else:
            pjQ = Q
        #f_{u,pjQ}([p^i]P)
        f_u_pjQ_piP, upjQ = miller_function_ate(pjQ, piP, 0, u)
        # f_{u,-[p^i]P}(pjQ) will also compute -u*[p^i]P
        f_u_piP_pjQ, upiP = miller_function_ate(-piP, pjQ, 0, u)

        piP = P.curve() (upiP[0]/upiP[3], -upiP[1]/(upiP[2]*upiP[3])) # p*P in Jacobian coordinates

        g = g * f_u_piP_pjQ * f_u_pjQ_piP

    gpk2 = g.frobenius(k//2)
    inv_g = 1/g
    m = gpk2*inv_g # g^(q^{k//2}-1)

    return m

### BLS-9

def weil_bls9(P, Q, b, r):
    """
    INPUT:
    - `P` on E(Fp)
    - `Q` on E(Fp9)
    - `b` curve coefficient
    - `r` order of points

    Computes (-f_{r,Q}(P) / f_{r,P}(Q))^(p^3-1) (the sign -1 vanishes)
    """
    f_r_Q_P, uQ = miller_function_tate_cln_a0_cubic_twist(Q, P, b, r)
    f_r_P_Q, uP = miller_function_tate_cln_a0_cubic_twist(P, Q, b, r)
    g = f_r_Q_P / f_r_P_Q
    return final_exp_easy_k9(g) # g.frobenius(3)/g

def alpha_weil_bls9(P, Q, b, u):
    """
    INPUT:
    - `P` on E(Fp)
    - `Q` on E(Fp9)
    - `b` curve coefficient
    - `u` seed

    Computes (f_{u3,Q}(P) / f_{u3,P}(Q))^(p^3-1)
    """
    u3 = u**3
    f_u_Q_P, uQ = miller_function_ate_cln_a0_cubic_twist(Q, P, b, u3)
    f_u_P_Q, uP = miller_function_ate_cln_a0_cubic_twist(P, Q, b, u3)
    g = f_u_Q_P / f_u_P_Q
    return final_exp_easy_k9(g)

### BLS-15

def weil_bls15(P, Q, b, r):
    """
    INPUT:
    - `P` on E(Fp)
    - `Q` on E(Fp15)
    - `b` curve coefficient
    - `r` order of points

    Computes (f_{r,Q}(P) / f_{r,P}(Q))^(p^5-1)*(p^2+p+1)
    """
    f_r_Q_P, uQ = miller_function_tate_cln_a0_cubic_twist(Q, P, b, r)
    f_r_P_Q, uP = miller_function_tate_cln_a0_cubic_twist(P, Q, b, r)
    g = f_r_Q_P / f_r_P_Q
    return final_exp_easy_k15(g)

def alpha_weil_bls15(P, Q, b, u):
    """
    INPUT:
    - `P` on E(Fp)
    - `Q` on E(Fp15)
    - `b` curve coefficient
    - `u` seed

    Computes (f_{u5,Q}(P) / (f_{u5,P}(Q))^(p^5-1)*(p^2+p+1)
    """
    u5 = u**5
    f_u_Q_P, uQ = miller_function_ate_cln_a0_cubic_twist(Q, P, b, u5)
    f_u_P_Q, uP = miller_function_ate_cln_a0_cubic_twist(P, Q, b, u5)
    g = f_u_Q_P / f_u_P_Q
    return final_exp_easy_k15(g)

### BLS-27

def weil_bls27(P, Q, b, r):
    """
    INPUT:
    - `P` on E(Fp)
    - `Q` on E(Fp15)
    - `b` curve coefficient
    - `r` order of points

    Computes (f_{r,Q}(P) / f_{r,P}(Q))^(p^9-1)
    """
    f_r_Q_P, uQ = miller_function_tate_cln_a0_cubic_twist(Q, P, b, r)
    f_r_P_Q, uP = miller_function_tate_cln_a0_cubic_twist(P, Q, b, r)
    g = f_r_Q_P / f_r_P_Q
    return final_exp_easy_k27(g)

def alpha_weil_bls27(P, Q, b, u):
    """
    INPUT:
    - `P` on E(Fp)
    - `Q` on E(Fp27)
    - `b` curve coefficient
    - `u` seed

    Computes (f_{u9,Q}(P) / (f_{u9,P}(Q))^(p^9-1)
    """
    u9 = u**9
    f_u_Q_P, uQ = miller_function_ate_cln_a0_cubic_twist(Q, P, b, u9)
    f_u_P_Q, uP = miller_function_ate_cln_a0_cubic_twist(P, Q, b, u9)
    g = f_u_Q_P / f_u_P_Q
    return final_exp_easy_k27(g)
