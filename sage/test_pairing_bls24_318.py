from sage.all_cmdline import *   # import sage library

from sage.rings.integer_ring import ZZ
from sage.rings.rational_field import QQ
from sage.misc.functional import cyclotomic_polynomial
from sage.rings.finite_rings.finite_field_constructor import FiniteField, GF
from sage.schemes.elliptic_curves.constructor import EllipticCurve

# this is much much faster with this statement:
# proof.arithmetic(False)
from sage.structure.proof.all import arithmetic

from pairing import *
from pairing_weil_bls import *
from test_pairing import *
from test_pairing_weil_bls import test_bilinearity_beta_weil_bls_even
from cost_pairing import cost_pairing_bls24

from test_scalar_mult import test_glv_scalar_mult_g1

if __name__ == "__main__":
    arithmetic(False)
    #preparse("QQx.<x> = QQ[]")
    QQx = QQ['x']; (x,) = QQx._first_ngens(1)
    # BLS24-318 seed
    u0 = ZZ(-0xeffff000)
    assert u0 == -2**32+2**28+2**12
    cost_pairing_bls24(u0)
    # the problem with BLS24 is the cost of the final exponentiation, and the size of G2.
    # BLS24 polynomials
    px = (x-1)**2*(x**8 - x**4 + 1)/3 + x
    rx = x**8-x**4+1
    tx = x+1
    cx = (x-1)**2/3
    yx = (x-1)*(2*x**4 - 1)/3
    betax = x**9 - 3*x**8 + 4*x**7 - 4*x**6 + 3*x**5 - 2*x**3 + 2*x**2 - x + 1
    lambx = x**4 -1
    c2x = (x**32 - 8*x**31 + 28*x**30 - 56*x**29 + 67*x**28 - 32*x**27 - 56*x**26 + 160*x**25 - 203*x**24 + 132*x**23 + 12*x**22 - 132*x**21 + 170*x**20 - 124*x**19 + 44*x**18 - 4*x**17 + 2*x**16 + 20*x**15 - 46*x**14 + 20*x**13 + 5*x**12 + 24*x**11 - 42*x**10 + 48*x**9 - 101*x**8 + 100*x**7 + 70*x**6 - 128*x**5 + 70*x**4 - 56*x**3 - 44*x**2 + 40*x + 100)/81 # cofactor for G2
    D = 3 # discriminant (-D = -3)
    exponent_easy = (px**12-1)*(px**4+1)
    exponent_hard = 3*(px**8-px**4+1)//rx
    print("exponent_easy = (px**12-1)*(px**4+1)")
    print("exponent_hard = 3*(px**8-px**4+1)//rx = {}".format(exponent_hard))
    
    p = ZZ(px(u0))
    r = ZZ(rx(u0))
    c = ZZ(cx(u0))
    c2 = ZZ(c2x(u0))
    t = ZZ(tx(u0))
    y = ZZ(yx(u0))
    print("p={:#x} # {} bits".format(p, p.nbits()))
    print("r={:#x} # {} bits".format(r, r.nbits()))
    Fp = GF(p, proof=False)
    E = EllipticCurve([Fp(0), Fp(5)])
    lambda_mod_r = ZZ(lambx(u0))
    beta_mod_p = Fp(betax(u0))

    Fpz = Fp['z']; (z,) = Fpz._first_ngens(1)
    #Fp2 = Fp.extension(z**2+1 , names=('i',)); (i,) = Fp2._first_ngens(1)
    #Fp2w = Fp2['w']; (w,) = Fp2w._first_ngens(1)
    #Fp4 = Fp2.extension(w**2-(i+1) , names=('j',)); (j,) = Fp4._first_ngens(1)
    #Fp4s = Fp4['s']; (s,) = Fp4s._first_ngens(1)
    # w^2 = i+1 <=> w^2-1 = i <=> (w^2-1)^2 = -1 <=> w^4 -2*w^2 + 2 = 0
    Fp4 = Fp.extension(z**4-2*z**2+2 , names=('ii',)); (ii,) = Fp4._first_ngens(1)
    Fp4s = Fp4['s']; (s,) = Fp4s._first_ngens(1)
    Fp24M = Fp4.extension(s**6 - (ii+4), names=('wM',)); (wM,) = Fp24M._first_ngens(1)
    Fq6M = Fp24M
    xiM = ii+4
    EM = EllipticCurve([Fp4(0),Fp4(5*(ii+4))])
    E24M = EllipticCurve([Fp24M(0), Fp24M(5)])
    # w^6 = ii+4 <=> w^6-4 = ii <=> (w^6-4)^4 - 2*(w^6-4)^2 + 2 = 0
    assert (wM**6-4)**4-2*(wM**6-4)**2+2 == 0
    Fp24M_A = Fp.extension((z**6-4)**4-2*(z**6-4)**2+2, names=('SM',)); (SM,) = Fp24M_A._first_ngens(1)
    assert (SM**6-4)**4-2*(SM**6-4)**2+2 == 0
    E24M_A = EllipticCurve([Fp24M_A(0), Fp24M_A(5)])

    try:
        test_xiM = -Fp24M.modulus().constant_coefficient()
        print("xiM == -Fp24M.modulus().constant_coefficient(): {}".format(xiM == test_xiM))
    except AttributeError as err:
        print("xiM = -Fp24M.modulus().constant_coefficient() raised an error:\n{}".format(err))
    try:
        test_xiM = -Fp24M.polynomial().constant_coefficient() # works only for absolute extensions on prime fields
        print("xiM == -Fp24M.polynomial().constant_coefficient(): {}".format(xiM == test_xiM))
    except AttributeError as err:
        print("xiM = -Fp24M.polynomial().constant_coefficient() raised an error:\n{}".format(err))

    def map_Fp24M_Fp24M_A(x):
        # evaluate elements of Fp24M = Fp[ii]/(ii^4-2*ii^2+2)[s]/(s^6-(ii+4)) at ii=S^6-4 and s=S
        # i <-> s^6-4 = SM^6-4 and s <-> SM
        #return sum([sum([yj*(SM**6-4)**j for j,yj in enumerate(xi.polynomial())]) * SM**e for e,xi in enumerate(x.list())])
        return sum([xi.polynomial()(SM**6-4) * SM**e for e,xi in enumerate(x.list())])
    def map_Fq6M_Fp24M_A(x, aM):
        return sum([xi.polynomial()(aM**6-4) * aM**e for e,xi in enumerate(x.list())])
    def map_Fp4_Fp24M_A(x):
        # evaluate elements of Fq=Fp[ii] at ii=s^6-4 = S^6-4
        return x.polynomial()(SM**6-4)

    Fp24D = Fp4.extension(s**6 - 5*ii, names=('wD',)); (wD,) = Fp24D._first_ngens(1)
    Fq6D = Fp24D
    xiD = 5*ii
    # w^6 = 5*ii <=> w^6/5 = ii <=> (w^6/5)^4 -2*(w^6/5)^2 + 2 = 0 ( --> *5^4)
    # <=> w^24 -2*5^2*w^12 + 2*5^4 = 0
    Fp24D_A = Fp.extension(z**24-2*5**2*z**12+2*5**4, names=('SD',)); (SD,) = Fp24D_A._first_ngens(1)
    assert wD**24-2*5**2*wD**12+2*5**4 == 0
    assert SD**24-2*5**2*SD**12+2*5**4 == 0
    ED = EllipticCurve([Fp4(0),Fp4(1/ii)]) # 5/(5*ii) -> M-twist
    E24D = EllipticCurve([Fp24D(0), Fp24D(5)])
    E24D_A = EllipticCurve([Fp24D_A(0), Fp24D_A(5)])

    try:
        test_xiD = -Fp24D.modulus().constant_coefficient()
        print("xiD == -Fp24D.modulus().constant_coefficient(): {}".format(xiD == test_xiD))
    except AttributeError as err:
        print("xiD = -Fp24D.modulus().constant_coefficient() raised an error:\n{}".format(err))
    try:
        test_xiD = -Fp24D.polynomial().constant_coefficient() # works only for absolute extensions on prime fields
        print("xiD == -Fp24D.polynomial().constant_coefficient(): {}".format(xiD == test_xiD))
    except AttributeError as err:
        print("xiD = -Fp24D.polynomial().constant_coefficient() raised an error:\n{}".format(err))

    def map_Fp24D_Fp24D_A(x):
        # evaluate elements of Fp24D = Fp[ii]/(ii^4-2*ii^2+2)[s]/(s^6-5*ii) at ii=S^6/5 and s=S
        # ii <-> wD^6/5 = SD^6/5 and wD <-> SD
        #return sum([sum([yj*(SD**6/5)**j for j,yj in enumerate(xi.polynomial())]) * SD**i for i,xi in enumerate(x.list())])
        return sum([xi.polynomial()(SD**6/5) * SD**e for e,xi in enumerate(x.list())])
    def map_Fq6D_Fp24D_A(x, aD):
        return sum([xi.polynomial()(aD**6/5) * aD**e for e,xi in enumerate(x.list())])
    def map_Fp4_Fp24D_A(x):
        # evaluate elements of Fq=Fp[ii] at ii=wD^6/5 = SD^6/5
        return x.polynomial()(SD**6/5)
    
    print("test E (G1)")
    test_order(E,r*c)
    print("test E' (G2) M-twist")
    test_order(EM,r*c2)
    print("test E' (G2) D-twist")
    test_order(ED,r*c2)
    
    print("test Frobenius map on G2 with M-twist")
    test_g2_frobenius_eigenvalue(E24M_A,EM,Fp24M,map_Fq6M_Fp24M_A,r,c2,D_twist=False)
    test_g2_frobenius_eigenvalue_alt(E24M_A,EM,map_Fp4_Fp24M_A,r,c2,D_twist=False)
    print("test Frobenius map on G2 with D-twist")
    test_g2_frobenius_eigenvalue(E24D_A,ED,Fp24D,map_Fq6D_Fp24D_A,r,c2,D_twist=True)
    test_g2_frobenius_eigenvalue_alt(E24D_A,ED,map_Fp4_Fp24D_A,r,c2,D_twist=True)
    
    print("test GLV on G1")
    test_glv_scalar_mult_g1(E, lambda_mod_r, beta_mod_p, r, c)

    print("test Miller M-twist")
    test_miller_function_tate(E, E24M, EM, r, c, c2, D_twist=False)
    test_miller_function_tate_2naf(E, E24M, EM, r, c, c2, D_twist=False)

    test_miller_function_ate(E,E24M,EM,r,c,c2,u0,D_twist=False)
    test_miller_function_ate_2naf(E,E24M,EM,r,c,c2,u0,D_twist=False)
    test_miller_function_ate_aklgl(E,EM,Fp24M,xiM,r,c,c2,u0,D_twist=False,verbose=False)
    test_miller_function_ate_2naf_aklgl(E,EM,Fp24M,xiM,r,c,c2,u0,D_twist=False,verbose=False)

    print("test Miller D-twist")
    test_miller_function_tate(E, E24D, ED, r, c, c2, D_twist=True)
    test_miller_function_tate_2naf(E, E24D, ED, r, c, c2, D_twist=True)

    test_miller_function_ate(E,E24D,ED,r,c,c2,u0,D_twist=True)
    test_miller_function_ate_2naf(E,E24D,ED,r,c,c2,u0,D_twist=True)
    test_miller_function_ate_aklgl(E,ED,Fp24D,xiD,r,c,c2,u0,D_twist=True,verbose=False)
    test_miller_function_ate_2naf_aklgl(E,ED,Fp24D,xiD,r,c,c2,u0,D_twist=True,verbose=False)
    
    print("\nFinal exponentiation")
    ee = ((px**12-1)*(px**4+1)*3*(px**8-px**4+1)//rx)(u0)
    test_final_exp_easy_k24(Fp24D_A)
    test_final_exp_bls24(Fp24D_A,r,u0,expected_exponent=ee)
    test_final_exp_2naf_bls24(Fp24D_A,r,u0,expected_exponent=ee)

    test_final_exp_easy_k24(Fp24M_A)
    test_final_exp_bls24(Fp24M_A,r,u0,expected_exponent=ee)
    test_final_exp_2naf_bls24(Fp24M_A,r,u0,expected_exponent=ee)
    
    print("\npairing")
    test_ate_pairing_bls24_aklgl(E,EM,r,c,c2,u0,Fp24M,map_Fp24M_Fp24M_A,D_twist=False)
    test_ate_pairing_bls24_aklgl(E,ED,r,c,c2,u0,Fp24D,map_Fp24D_Fp24D_A,D_twist=True)

    test_ate_pairing_bls24_2naf_aklgl(E,EM,r,c,c2,u0,Fp24M,map_Fp24M_Fp24M_A,D_twist=False)
    test_ate_pairing_bls24_2naf_aklgl(E,ED,r,c,c2,u0,Fp24D,map_Fp24D_Fp24D_A,D_twist=True)

    test_bilinearity_beta_weil_bls_even(E, E24M_A, EM, Fq6M, r, c, c2, u0, r, map_Fq6M_Fp24M_A, D_twist=False, function_name=weil_bls24, final_exp_hard=final_exp_hard_bls24)
    test_bilinearity_beta_weil_bls_even(E, E24D_A, ED, Fq6D, r, c, c2, u0, r, map_Fq6D_Fp24D_A, D_twist=True, function_name=weil_bls24, final_exp_hard=final_exp_hard_bls24)

    test_bilinearity_beta_weil_bls_even(E, E24M_A, EM, Fq6M, r, c, c2, u0, u0, map_Fq6M_Fp24M_A, D_twist=False, function_name=alpha_weil_bls24, final_exp_hard=final_exp_hard_bls24)
    test_bilinearity_beta_weil_bls_even(E, E24D_A, ED, Fq6D, r, c, c2, u0, u0, map_Fq6D_Fp24D_A, D_twist=True, function_name=alpha_weil_bls24, final_exp_hard=final_exp_hard_bls24)

    test_bilinearity_beta_weil_bls_even(E, E24M_A, EM, Fq6M, r, c, c2, u0, u0, map_Fq6M_Fp24M_A, D_twist=False, function_name=beta_weil_bls24, final_exp_hard=final_exp_hard_bls24)
    test_bilinearity_beta_weil_bls_even(E, E24D_A, ED, Fq6D, r, c, c2, u0, u0, map_Fq6D_Fp24D_A, D_twist=True, function_name=beta_weil_bls24, final_exp_hard=final_exp_hard_bls24)

    test_bilinearity_beta_weil_bls_even(E, E24M_A, EM, Fq6M, r, c, c2, u0, u0, map_Fq6M_Fp24M_A, D_twist=False, function_name=beta_weil_bls24_kinoshita_suzuki, final_exp_hard=final_exp_hard_bls24)
    test_bilinearity_beta_weil_bls_even(E, E24D_A, ED, Fq6D, r, c, c2, u0, u0, map_Fq6D_Fp24D_A, D_twist=True, function_name=beta_weil_bls24_kinoshita_suzuki, final_exp_hard=final_exp_hard_bls24)
