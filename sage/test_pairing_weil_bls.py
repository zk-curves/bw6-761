from pairing import *
from pairing_weil_bls import *
from test_pairing import *

def test_bilinearity_beta_weil_cln_a0_cubic_twist(E, E_Fpk, E2, Fq3, r, c, c2, u, T, map_Fq3_Fpk, D_twist=False, function_name=weil_bls9, final_exp_hard=final_exp_hard_bls9):
    """
    Testing Beta Weil pairing for BLS-9 with cln_a0_cubic_twist formulas

    Compatible:
    weil_bls9(P, Q, b, r)
    alpha_weil_bls9(P, Q, b, u)

    weil_bls15(P, Q, b, r)
    alpha_weil_bls15(P, Q, b, u)

    weil_bls27(P, Q, b, r)
    alpha_weil_bls27(P, Q, b, u)

    where Q2 and P are both of order r and in E(Fq3) (with absolute extension)
    but in distinct subgroups
    To obtain valid Q2, first Q of order r is sampled from E2(Fq) then
    a map (D-twist or M-twist) is applied to Q to obtain Q2 in E_Fq3.
    Finally a map from Fq3 to Fp9 is applied.
    Works with 3-twists (Fqd = Fq3)

    INPUT:
    - `E`: elliptic curve over ground field GF(p) of order r*c
    - `E_Fpk`: elliptic curve over GF(p^k)
    - `E2`: d-twist over GF(q) where q = p^{k/d} of order r*c2
    - `Fq3`: extension of degree 3 on top of Fq
    - `r`: prime integer, order of subgroup of E and E2
    - `c`: cofactor of E, so that # E(Fp) = r*c
    - `c2`: cofactor of E2, so that # E2(Fq) = r*c2, and q = p^{k/d}
    - `u`: the seed of parameters
    - `T`: the seed for the Miller function (u or r)
    - `D_twist`: whether E2(Fq) is a D-twist or an M-twist of E(Fq)

    RETURN: true or False
    """
    P = c*E.random_element()
    while P == E(0):
        P = c * E.random_element()
    Q = c2*E2.random_element()
    while Q == E2(0):
        Q = c2 * E2.random_element()
    #Fq3 = E_Fq3.base_field() # assume that the degree d extension is explicit
    w = Fq3.gen(0)
    Fpk = E_Fpk.base_field()
    aX = Fpk.gen(0)
    #exponent = (Fq3.cardinality()-1) // r # (q^d-1)//r = (p^k-1)//r
    if not D_twist:
        Q2 = psi_sextic_m_twist(Q, w)
    else:
        Q2 = psi_sextic_d_twist(Q, w)
    Q2A = E_Fpk (map_Fq3_Fpk(Q2[0], aX), map_Fq3_Fpk(Q2[1], aX))
    f = function_name(P, Q2A, E.a6(), T)
    g = final_exp_hard(f, u)
    ok = f != 0
    ok_exp = g != 0
    aa = 1
    while (ok and ok_exp) and aa < 4:
        Pa = aa*P
        bb = 1
        while (ok and ok_exp) and bb < 4:
            Qb = bb*Q
            if not D_twist:
                Q2b = psi_sextic_m_twist(Qb, w)
            else:
                Q2b = psi_sextic_d_twist(Qb, w)
            Q2bA = E_Fpk (map_Fq3_Fpk(Q2b[0], aX), map_Fq3_Fpk(Q2b[1], aX))
            fab = function_name(Pa, Q2bA, E.a6(), T)
            gab = final_exp_hard(fab, u)
            ok = fab != 0 and fab == f**(aa*bb)
            ok_exp = gab != 0 and gab == g**(aa*bb)
            bb += 1
        aa += 1
    print("test bilinearity {}: {}".format(function_name.__name__, ok))
    print("test bilinearity {}: {} with {}".format(function_name.__name__, ok_exp, final_exp_hard.__name__))
    return ok

def test_bls_odd(k, u, r, c, c2, ee, E, EM, E_Fq3M, E_FpkM, Fq3M, ED, E_Fq3D, E_FpkD, Fq3D, map_Fq3_Fpk):

    p = E.base_field().characteristic()
    print("BLS{}-{} u = {:#x}".format(k, p.nbits(), u))
    if u < 0:
        bits_2naf_u = bits_2naf(-u)
        print("u = -(" + "".join(["{:+}^{}".format(2*bits_2naf_u[i], i) for i in range(len(bits_2naf_u)) if bits_2naf_u[i] != 0]) + ")")
    else:
        bits_2naf_u = bits_2naf(u)
        print("u = " + "".join(["{:+}^{}".format(2*bits_2naf_u[i], i) for i in range(len(bits_2naf_u)) if bits_2naf_u[i] != 0]))
    print(E)

    if not r.is_prime():
        print("error r is not prime, please provide cofactor")
        g = gcd(r, prod(prime_range(10**6)))
        smooth_part = 1
        s = r
        while g > 1:
            smooth_part = smooth_part*g
            s = s//g
            g = gcd(s, g)
        print("a factor is {}".format(smooth_part.factor()))
        return

    test_order_twist(EM, r, c2, D_twist=False)
    test_order_twist(ED, r, c2, D_twist=True)

    test_g2_frobenius_eigenvalue(E_FpkM, EM, Fq3M, map_Fq3_Fpk, r, c2, D_twist=False)
    test_g2_frobenius_eigenvalue(E_FpkD, ED, Fq3D, map_Fq3_Fpk, r, c2, D_twist=True)

    test_double_line_cln_a0_cubic_twist(E, EM, Fq3M, D_twist=False)
    test_add_line_cln_a0_cubic_twist(E, EM, Fq3M, D_twist=False)
    test_add_line_cln_a0_cubic_twist_with_z(E, EM, Fq3M, D_twist=False)

    test_double_line_cln_a0_cubic_twist(E, ED, Fq3D, D_twist=True)
    test_add_line_cln_a0_cubic_twist(E, ED, Fq3D, D_twist=True)
    test_add_line_cln_a0_cubic_twist_with_z(E, ED, Fq3D, D_twist=True)

    t_1 = u

    test_bilinearity_miller_loop_ate_cln_a0_cubic_twist(E, E_Fq3M, EM, r, c, c2, r, D_twist=False, function_name=miller_function_tate_cln_a0_cubic_twist, Tate=True)
    test_bilinearity_miller_loop_ate_cln_a0_cubic_twist(E, E_Fq3D, ED, r, c, c2, r, D_twist=True, function_name=miller_function_tate_cln_a0_cubic_twist, Tate=True)
    test_bilinearity_miller_loop_ate_cln_a0_cubic_twist(E, E_Fq3M, EM, r, c, c2, r, D_twist=False, function_name=miller_function_tate_cln_a0_cubic_twist, Tate=False)
    test_bilinearity_miller_loop_ate_cln_a0_cubic_twist(E, E_Fq3D, ED, r, c, c2, r, D_twist=True, function_name=miller_function_tate_cln_a0_cubic_twist, Tate=False)

    test_bilinearity_miller_loop_ate_cln_a0_cubic_twist(E, E_Fq3M, EM, r, c, c2, t_1, D_twist=False, function_name=miller_function_ate_cln_a0_cubic_twist)
    test_bilinearity_miller_loop_ate_cln_a0_cubic_twist(E, E_Fq3D, ED, r, c, c2, t_1, D_twist=True, function_name=miller_function_ate_cln_a0_cubic_twist)

    test_bilinearity_miller_loop_ate_cln_a0_cubic_twist(E, E_Fq3M, EM, r, c, c2, t_1, D_twist=False, function_name=miller_function_ate_2naf_cln_a0_cubic_twist)
    test_bilinearity_miller_loop_ate_cln_a0_cubic_twist(E, E_Fq3D, ED, r, c, c2, t_1, D_twist=True, function_name=miller_function_ate_2naf_cln_a0_cubic_twist)

    print("\nFinal exponentiation")
    FpkM = E_FpkM.base_field()
    FpkD = E_FpkD.base_field()

    if k == 9:
        test_final_exp_easy_k9(FpkM)
        test_final_exp_hard_bls9(FpkM, u, r, exponent_hard=ee)
        test_final_exp_easy_k9(FpkD)
        test_final_exp_hard_bls9(FpkD, u, r, exponent_hard=ee)
    elif k == 15:
        test_final_exp_easy_k15(FpkM)
        test_final_exp_hard_bls15(FpkM, u, r, exponent_hard=ee)
        test_final_exp_easy_k15(FpkD)
        test_final_exp_hard_bls15(FpkD, u, r, exponent_hard=ee)
    elif k == 27:
        test_final_exp_easy_k27(FpkM)
        test_final_exp_hard_bls27(FpkM, u, r, exponent_hard=ee)
        test_final_exp_easy_k27(FpkD)
        test_final_exp_hard_bls27(FpkD, u, r, exponent_hard=ee)

    if k == 9:
        test_bilinearity_beta_weil_cln_a0_cubic_twist(E, E_FpkM, EM, Fq3M, r, c, c2, u, r, map_Fq3_Fpk, D_twist=False, function_name=weil_bls9, final_exp_hard=final_exp_hard_bls9)
        test_bilinearity_beta_weil_cln_a0_cubic_twist(E, E_FpkD, ED, Fq3D, r, c, c2, u, r, map_Fq3_Fpk, D_twist=True, function_name=weil_bls9, final_exp_hard=final_exp_hard_bls9)
        test_bilinearity_beta_weil_cln_a0_cubic_twist(E, E_FpkM, EM, Fq3M, r, c, c2, u, u, map_Fq3_Fpk, D_twist=False, function_name=alpha_weil_bls9, final_exp_hard=final_exp_hard_bls9)
        test_bilinearity_beta_weil_cln_a0_cubic_twist(E, E_FpkD, ED, Fq3D, r, c, c2, u, u, map_Fq3_Fpk, D_twist=True, function_name=alpha_weil_bls9, final_exp_hard=final_exp_hard_bls9)
    elif  k == 15:
        test_bilinearity_beta_weil_cln_a0_cubic_twist(E, E_FpkM, EM, Fq3M, r, c, c2, u, r, map_Fq3_Fpk, D_twist=False, function_name=weil_bls15, final_exp_hard=final_exp_hard_bls15)
        test_bilinearity_beta_weil_cln_a0_cubic_twist(E, E_FpkD, ED, Fq3D, r, c, c2, u, r, map_Fq3_Fpk, D_twist=True, function_name=weil_bls15, final_exp_hard=final_exp_hard_bls15)
        test_bilinearity_beta_weil_cln_a0_cubic_twist(E, E_FpkM, EM, Fq3M, r, c, c2, u, u, map_Fq3_Fpk, D_twist=False, function_name=alpha_weil_bls15, final_exp_hard=final_exp_hard_bls15)
        test_bilinearity_beta_weil_cln_a0_cubic_twist(E, E_FpkD, ED, Fq3D, r, c, c2, u, u, map_Fq3_Fpk, D_twist=True, function_name=alpha_weil_bls15, final_exp_hard=final_exp_hard_bls15)
    elif  k == 27:
        test_bilinearity_beta_weil_cln_a0_cubic_twist(E, E_FpkM, EM, Fq3M, r, c, c2, u, r, map_Fq3_Fpk, D_twist=False, function_name=weil_bls27, final_exp_hard=final_exp_hard_bls27)
        test_bilinearity_beta_weil_cln_a0_cubic_twist(E, E_FpkD, ED, Fq3D, r, c, c2, u, r, map_Fq3_Fpk, D_twist=True, function_name=weil_bls27, final_exp_hard=final_exp_hard_bls27)
        test_bilinearity_beta_weil_cln_a0_cubic_twist(E, E_FpkM, EM, Fq3M, r, c, c2, u, u, map_Fq3_Fpk, D_twist=False, function_name=alpha_weil_bls27, final_exp_hard=final_exp_hard_bls27)
        test_bilinearity_beta_weil_cln_a0_cubic_twist(E, E_FpkD, ED, Fq3D, r, c, c2, u, u, map_Fq3_Fpk, D_twist=True, function_name=alpha_weil_bls27, final_exp_hard=final_exp_hard_bls27)

def test_bilinearity_beta_weil_bls_even(E, E_Fpk, E2, Fqd, r, c, c2, u, T, map_Fqd_Fpk, D_twist=False, function_name=beta_weil_bls12, final_exp_hard=final_exp_hard_bls12):
    """
    Testing Beta Weil pairing for BLS-k with even k

    Compatible:
    beta_weil_bls12(P, Q, b, u)
    weil_bls12(P, Q, b, r)
    beta_weil_bls12_kinoshita_suzuki(P, Q, b, u)

    beta_weil_bls24(P, Q, b, u)
    weil_bls24(P, Q, b, r)
    beta_weil_bls24_kinoshita_suzuki(P, Q, b, u)

    #beta_weil_bls_even_k(k, P, Q, b, u)
    #beta_weil_bls_even_k_kinoshita_suzuki(k, P, Q, b, u)

    where Q2 and P are both of order r and in E(Fqd) (with absolute extension)
    but in distinct subgroups
    To obtain valid Q2, first Q of order r is sampled from E2(Fq) then
    a map (D-twist or M-twist) is applied to Q to obtain Q2 in E_Fqd.
    Finally a map from Fqd to Fpk is applied.
    Works with 6-twists (Fqd = Fq6)
    Should work with quadratic twists and quartic twists (Fqd = Fq2, Fqd = Fq4)

    INPUT:
    - `E`: elliptic curve over ground field GF(p) of order r*c
    - `E_Fpk`: elliptic curve over GF(p^k)
    - `E2`: d-twist over GF(q) where q = p^{k/d} of order r*c2
    - `r`: prime integer, order of subgroup of E and E2
    - `c`: cofactor of E, so that # E(Fp) = r*c
    - `c2`: cofactor of E2, so that # E2(Fq) = r*c2, and q = p^{k/d}
    - `u`: the seed of parameters
    - `T`: the seed for the Miller function (u or r)
    - `D_twist`: whether E2(Fq) is a D-twist or an M-twist of E(Fq)

    RETURN: true or False
    """
    P = c*E.random_element()
    while P == E(0):
        P = c * E.random_element()
    Q = c2*E2.random_element()
    while Q == E2(0):
        Q = c2 * E2.random_element()
    #Fqd = E_Fqd.base_field() # assume that the degree d extension is explicit
    w = Fqd.gen(0)
    Fpk = E_Fpk.base_field()
    aX = Fpk.gen(0)
    #exponent = (Fqd.cardinality()-1) // r # (q^d-1)//r = (p^k-1)//r
    if not D_twist:
        Q2 = psi_sextic_m_twist(Q, w)
    else:
        Q2 = psi_sextic_d_twist(Q, w)
    Q2A = E_Fpk (map_Fqd_Fpk(Q2[0], aX), map_Fqd_Fpk(Q2[1], aX))
    f = function_name(P, Q2A, E.a6(), T)
    g = final_exp_hard(f, u)
    ok = f != 0
    ok_exp = g != 0
    aa = 1
    while (ok and ok_exp) and aa < 4:
        Pa = aa*P
        bb = 1
        while (ok and ok_exp) and bb < 4:
            Qb = bb*Q
            if not D_twist:
                Q2b = psi_sextic_m_twist(Qb, w)
            else:
                Q2b = psi_sextic_d_twist(Qb, w)
            Q2bA = E_Fpk (map_Fqd_Fpk(Q2b[0], aX), map_Fqd_Fpk(Q2b[1], aX))
            fab = function_name(Pa, Q2bA, E.a6(), T)
            gab = final_exp_hard(fab, u)
            ok = fab != 0 and fab == f**(aa*bb)
            ok_exp = gab != 0 and gab == g**(aa*bb)
            bb += 1
        aa += 1
    print("test bilinearity {}: {}".format(function_name.__name__, ok))
    print("test bilinearity {}: {} with {}".format(function_name.__name__, ok_exp, final_exp_hard.__name__))
    return ok

