from sage.all_cmdline import *   # import sage library

from sage.rings.integer_ring import ZZ
from sage.rings.rational_field import QQ
from sage.misc.functional import cyclotomic_polynomial
from sage.rings.finite_rings.finite_field_constructor import FiniteField, GF
from sage.schemes.elliptic_curves.constructor import EllipticCurve

# this is much much faster with this statement:
# proof.arithmetic(False)
from sage.structure.proof.all import arithmetic

from pairing import *
from pairing_kss16 import *
from test_pairing import *
from test_pairing_kss16_330 import test_optimal_ate_formula, test_final_exp_hard_kss16
from cost_pairing import cost_pairing_kss16

if __name__ == "__main__":
    arithmetic(False)
    u0 = ZZ(2**35-2**32-2**18+2**8+1)
    # we need u=+-25 mod 70 to ensure px, rx to be integers.
    cost_pairing_kss16(u0)    

    QQx = QQ['x']; (x,) = QQx._first_ngens(1)
    k = 16
    D = 1
    px = (x**10 + 2*x**9 + 5*x**8 + 48*x**6 + 152*x**5 + 240*x**4 + 625*x**2 + 2398*x + 3125)/980
    rx = (x**8 + 48*x**4 + 625)/61250 # 625 = 5^4, 61250 = 2*5^4*7^2
    tx = (2*x**5 + 41*x + 35)/35
    yx = (x**5 + 5*x**4 + 38*x + 120)/70 # Y such that T^2 - 4*P = -4*Y^2
    betax = (x**9-11*x**8-16*x**7-120*x**6-32*x**5-498*x**4-748*x**3-2740*x**2-3115*x-5651)/4018
    lambx = (x**4 + 24)/7 # sqrt(-1) mod R
    exponent = (px**8+1)//rx

    cx = 125 * (x**2 + 2*x + 5)/2 # C such that P+1-T = C*R
    # for G2, compute #E(Fp4) then compute its 4-th twist
    print("tx^2-4*px/yx^2 = {}".format((tx**2 - 4*px)/yx**2))
    D = 4
    assert tx**2 - 4*px == -D*yx**2
    c2x = (x**32 + 8*x**31 + 44*x**30 + 152*x**29 + 550*x**28 + 2136*x**27 + 8780*x**26 + 28936*x**25 + 83108*x**24 + 236072*x**23 + 754020*x**22 + 2287480*x**21 + 5986066*x**20 + 14139064*x**19 + 35932740*x**18 + 97017000*x**17 + 237924870*x**16 + 498534968*x**15 + 1023955620*x**14 + 2353482920*x**13 + 5383092978*x**12 + 10357467880*x**11 + 17391227652*x**10 + 31819075896*x**9 + 65442538660*x**8 + 117077934360*x**7 + 162104974700*x**6 + 208762740168*x**5 + 338870825094*x**4 + 552745197960*x**3 + 632358687500*x**2 + 414961135000*x + 126854087873)/15059072

    # optimal ate pairing has Miller loop (f_{u,Q}(P) l_{[u]Q,\pi(Q)}(P))^{p^3} l_{Q,Q}(P)
    assert ((2 + x*px**3 + px**4) % rx) == 0
    # so 2*Q + u*pi_3(Q) + pi_4(Q) = 0
    
    p = ZZ(px(u0))
    r = ZZ(rx(u0))
    c = ZZ(cx(u0))
    c2 = ZZ(c2x(u0))
    t = ZZ(tx(u0))
    y = ZZ(yx(u0))
    Fp = GF(p, proof=False)
    E = EllipticCurve([Fp(1), Fp(0)])

    Fpz = Fp['z']; (z,) = Fpz._first_ngens(1)
    Fp4 = Fp.extension(z**4 -2 , names=('i',)); (i,) = Fp4._first_ngens(1)
    Fp4s = Fp4['s']; (s,) = Fp4s._first_ngens(1)
    
    Fp16D = Fp4.extension(s**4 -i , names=('wD',)); (wD,) = Fp16D._first_ngens(1)
    Fq4D = Fp16D
    xiD = i
    ED = EllipticCurve([Fp4(1/i), Fp4(0)])
    E_Fq4D = EllipticCurve([Fp16D(1), Fp16D(0)])
    # s^4=i => s^16 = i^4 = 2
    Fp16D_A = Fp.extension(z**16 - 2, names=('SD',)); (SD,) = Fp16D_A._first_ngens(1)
    E16D_A = EllipticCurve([Fp16D_A(1), Fp16D_A(0)])

    def map_Fp16D_Fp16D_A(X):
        # evaluate elements of Fp16D = Fp[i]/(i^4-2)[s]/(s^4-i) at i=S^4 and s=S
        # i <-> wD^4 = SD^4 and wD <-> SD
        #return sum([sum([yj*(SD**4)**j for j,yj in enumerate(xi.polynomial())]) * SD**i for i,xi in enumerate(x.list())])
        return sum([xi.polynomial()(SD**4) * SD**e for e,xi in enumerate(X.list())])
    def map_Fq4D_Fp16D_A(X, aD):
        return sum([xi.polynomial()(aD**4) * aD**e for e,xi in enumerate(X.list())])

    def map_Fp4_Fp16D_A(X):
        # evaluate elements of Fq=Fp[i] at i=wD^4 = SD^4
        return X.polynomial()(SD**4)

    Fp16M = Fp4.extension(s**4 -(i+4) , names=('wM',)); (wM,) = Fp16M._first_ngens(1)
    Fq4M = Fp16M
    xiM = i+4
    EM = EllipticCurve([Fp4(i+4), Fp4(0)])
    E_Fq4M = EllipticCurve([Fp16M(1), Fp16M(0)])
    print("test EM (G2)")
    test_order(EM,r*c2)
    # s^4-4=i => (s^4-4)^4 = i^4 = 2
    Fp16M_A = Fp.extension((z**4-4)**4-2, names=('SM',)); (SM,) = Fp16M_A._first_ngens(1)
    E16M_A = EllipticCurve([Fp16M_A(1), Fp16M_A(0)])

    def map_Fp16M_Fp16M_A(X):
        # evaluate elements of Fp16M = Fp[i]/(i^4-2)[s]/(s^4-(i+4)) at i=S^4-4 and s=S
        # i <-> s^4-4 = SM^4-4 and s <-> SM
        #return sum([sum([yj*(SM**4-4)**j for j,yj in enumerate(xi.polynomial())]) * SM**e for e,xi in enumerate(X.list())])
        return sum([xi.polynomial()(SM**4-4) * SM**e for e,xi in enumerate(X.list())])
    def map_Fq4M_Fp16M_A(X, aM):
        return sum([xi.polynomial()(aM**4-4) * aM**e for e,xi in enumerate(X.list())])
    def map_Fp4_Fp16M_A(X):
        # evaluate elements of Fq=Fp[i] at i=s^4-4 = S^4-4
        return X.polynomial()(SM**4-4)

    print("test map_Fp16M_Fp16M_A")
    x0 = Fp16M.random_element()
    x1 = map_Fp16M_Fp16M_A(x0)

    print("test map_Fp16D_Fp16D_A")
    x0 = Fp16D.random_element()
    x1 = map_Fp16D_Fp16D_A(x0)

    print("test optimal ate pairing formula")
    test_optimal_ate_formula(E16M_A, E_Fq4M, EM, map_Fp16M_Fp16M_A, wM, u0, r, c2, D_twist=False)
    test_optimal_ate_formula(E16D_A, E_Fq4D, ED, map_Fp16D_Fp16D_A, wD, u0, r, c2, D_twist=True)

    print("test E (G1)")
    test_order(E,r*c)
    print("test ED (G2)")
    test_order(ED,r*c2)
    print("test EM (G2)")
    test_order(EM,r*c2)

    print("test Frobenius map on G2 with M-twist")
    test_g2_frobenius_eigenvalue(E16M_A,EM,Fp16M,map_Fq4M_Fp16M_A,r,c2,D_twist=False)
    test_g2_frobenius_eigenvalue_alt(E16M_A,EM,map_Fp4_Fp16M_A,r,c2,D_twist=False)
    print("test Frobenius map on G2 with D-twist")
    test_g2_frobenius_eigenvalue(E16D_A,ED,Fp16D,map_Fq4D_Fp16D_A,r,c2,D_twist=True)
    test_g2_frobenius_eigenvalue_alt(E16D_A,ED,map_Fp4_Fp16D_A,r,c2,D_twist=True)

    print("tests with D-twist")
    test_double_line_j(E, ED, Fq4D, D_twist=True)
    test_add_line_j(E, ED, Fq4D, D_twist=True)
    test_double_line_cln_b0(E, ED, Fq4D, D_twist=True)
    test_add_line_cln_b0(E, ED, Fq4D, D_twist=True)
    test_add_line_cln_b0_with_z(E, ED, Fq4D, D_twist=True)

    test_miller_function_ate(E, E_Fq4D, ED, r, c, c2, t-1, D_twist=True)
    test_miller_function_ate_2naf(E, E_Fq4D, ED, r, c, c2, t-1, D_twist=True)
    test_miller_function_ate_csb(E, E_Fq4D, ED, r, c, c2, t-1, D_twist=True)
    test_miller_function_ate_cln_b0(E, E_Fq4D, ED, r, c, c2, t-1, D_twist=True)
    test_miller_function_ate_2naf_cln_b0(E, E_Fq4D, ED, r, c, c2, t-1, D_twist=True)

    test_bilinearity_miller_loop_ate_absolute_extension(E, ED, Fp16D, Fp16D_A, map_Fp16D_Fp16D_A, r, c, c2, u0, D_twist=True, function_name=miller_loop_opt_ate_kss16)
    test_bilinearity_miller_loop_ate_absolute_extension(E, ED, Fp16D, Fp16D_A, map_Fp16D_Fp16D_A, r, c, c2, u0, D_twist=True, function_name=miller_loop_opt_ate_kss16_v2)

    test_bilinearity_miller_loop_ate_absolute_extension(E, ED, Fp16D, Fp16D_A, map_Fp16D_Fp16D_A, r, c, c2, u0, D_twist=True, function_name=miller_loop_opt_ate_kss16_cln_b0)
    test_bilinearity_miller_loop_ate_absolute_extension(E, ED, Fp16D, Fp16D_A, map_Fp16D_Fp16D_A, r, c, c2, u0, D_twist=True, function_name=miller_loop_opt_ate_kss16_cln_b0_v2)

    print("tests with M-twist")
    test_double_line_j(E, EM, Fq4M, D_twist=False)
    test_add_line_j(E, EM, Fq4M, D_twist=False)
    test_double_line_cln_b0(E, EM, Fq4M, D_twist=False)
    test_add_line_cln_b0(E, EM, Fq4M, D_twist=False)
    test_add_line_cln_b0_with_z(E, EM, Fq4M, D_twist=False)

    test_miller_function_ate(E, E_Fq4M, EM, r, c, c2, t-1, D_twist=False)
    test_miller_function_ate_2naf(E, E_Fq4M, EM, r, c, c2, t-1, D_twist=False)
    test_miller_function_ate_csb(E, E_Fq4M, EM, r, c, c2, t-1, D_twist=False)
    test_miller_function_ate_cln_b0(E, E_Fq4M, EM, r, c, c2, t-1, D_twist=False)
    test_miller_function_ate_2naf_cln_b0(E, E_Fq4M, EM, r, c, c2, t-1, D_twist=False)

    test_bilinearity_miller_loop_ate_absolute_extension(E, EM, Fp16M, Fp16M_A, map_Fp16M_Fp16M_A, r, c, c2, u0, D_twist=False, function_name=miller_loop_opt_ate_kss16)
    test_bilinearity_miller_loop_ate_absolute_extension(E, EM, Fp16M, Fp16M_A, map_Fp16M_Fp16M_A, r, c, c2, u0, D_twist=False, function_name=miller_loop_opt_ate_kss16_v2)

    test_bilinearity_miller_loop_ate_absolute_extension(E, EM, Fp16M, Fp16M_A, map_Fp16M_Fp16M_A, r, c, c2, u0, D_twist=False, function_name=miller_loop_opt_ate_kss16_cln_b0)
    test_bilinearity_miller_loop_ate_absolute_extension(E, EM, Fp16M, Fp16M_A, map_Fp16M_Fp16M_A, r, c, c2, u0, D_twist=False, function_name=miller_loop_opt_ate_kss16_cln_b0_v2)

    print("Test Final Exp")
    test_final_exp_easy_k16(Fp16D_A)
    test_final_exp_easy_k16(Fp16M_A)
    expected_exponent = ((p**8 + 1)//r) * (-14)*(u0//5)**3 # -14/125*x^3
    test_final_exp_hard_kss16(Fp16M_A, r, u0, function_name=final_exp_hard_kss16, expected_exponent=expected_exponent)
    test_final_exp_hard_kss16(Fp16D_A, r, u0, function_name=final_exp_hard_kss16, expected_exponent=expected_exponent)
    test_final_exp_hard_kss16(Fp16M_A, r, u0, function_name=final_exp_hard_kss16_ghammam, expected_exponent=-expected_exponent)
    test_final_exp_hard_kss16(Fp16D_A, r, u0, function_name=final_exp_hard_kss16_ghammam, expected_exponent=-expected_exponent)
