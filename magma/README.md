# Overview
This directory contains Magma implementation of `BW6-761` curve for formulas verification.

# Directory structure

The high-level structure of the repository is as follows.

* `pairing.mag` contains formulas code and
* `script_BW6_761.mag` contains tests.

# Build guide

Install [Magma](http://magma.maths.usyd.edu.au/magma/faq/install).

## Test
```bash
magma script_BW6_761.mag
```
