/*
Magma script to generate formulas for optimal ate pairing,
optimal Miller loop,
(Vercauteren 2010 IEEE, DOI 10.1109/TIT.2009.2034881 https://eprint.iacr.org/2008/096)
efficient final exponentiation,
fast G1 and G2 membership testing,
fast clearing cofactor,
(Fuentes-Castaneda, L., Knapp, E., Rodriguez-Henriquez, F.: Faster hashing to G2.
In: Miri, A., Vaudenay, S. (eds.) SAC 2011. LNCS, vol. 7118, pp. 412–430. Springer
(Aug 2012). https://doi.org/10.1007/978-3-642-28496-0 25
http://cacr.uwaterloo.ca/techreports/2011/cacr2011-26.pdf)
GLV endomorphism.
(Smith'15 https://hal.inria.fr/hal-00874925 http://dx.doi.org/10.1090/conm/637)
*/

QQ := Rationals();
QQx<x> := PolynomialRing(QQ);
// seed of the curve BLS12-377, CP6_782 and BW6-761
u0 := 0x8508C00000000001;
u0 eq 2^63+2^58+2^56+2^51+2^47+2^46+1;

// polynomials of BLS12
r_BLS12 := CyclotomicPolynomial(12);
t_BLS12 := x+1;
c_BLS12 := (x-1)^2/3;
q_BLS12 := c_BLS12*r_BLS12 + (t_BLS12-1);

// polynomials of the curve BW6_761
t0x := x^5 - 3*x^4 + 3*x^3 - x + 3;
y0x := (x^5 - 3*x^4 + 3*x^3 - x + 3)/3;

qx := (103*x^12 - 379*x^11 + 250*x^10 + 691*x^9 - 911*x^8 - 79*x^7 + 623*x^6 - 640*x^5 + 274*x^4 + 763*x^3 + 73*x^2 + 254*x + 229)/9;

lambx := x^5 - 3*x^4 + 3*x^3 - x + 1;
omegx := (103*x^11 - 482*x^10 + 732*x^9 + 62*x^8 - 1249*x^7 + 1041*x^6 + 214*x^5 - 761*x^4 + 576*x^3 + 11*x^2 - 265*x + 66)/21;
tx := (13*x^6 - 23*x^5 - 9*x^4 + 35*x^3 + 10*x + 22)/3;
yx :=(9*x^6 - 17*x^5 - 3*x^4 + 21*x^3 + 8*x + 12)/3;
rx := (x^6 - 2*x^5 + 2*x^3 + x + 1)/3;
order := qx + 1 - tx;
assert IsPrime(ZZ ! (Evaluate(rx,u0)):Proof:=false);
assert IsPrime(ZZ ! (Evaluate(qx,u0)):Proof:=false);
ht := 13;
hy := 9;
assert qx eq ((t0x+ht*rx)^2 + 3*(y0x+hy*rx)^2)/4;

// optimal ate pairing computation from Vercauteren's paper
M := Matrix(QQx, 2, 2, [rx, 0, -qx mod rx, 1]);
R := LLL(M);
printf "reduced matrix for optimal ate pairing computation:\n";
print R;
assert ((R[1][1] + qx*R[1][2]) mod rx) eq 0;
assert ((R[2][1] + qx*R[2][2]) mod rx) eq 0;
printf "optimal ate pairing formula: f_{%o,Q}(P) * f_{%o,pi(Q)}(P) * l_{(%o)Q, (%o)pi(Q)}(P)\n", R[1][1], R[1][2], R[1][1], R[1][2];
printf "we have (%o) + (%o)*qx = %o mod rx\n", R[1][1], R[1][2], ((R[1][1] + qx*R[1][2]) mod rx);
printf "because (%o)P + (%o)pi(P) == 0 for all P of order r, the line l is a vertical and can be removed.\n", R[1][1], R[1][2];
printf "alternatively,\n";
printf "optimal ate pairing formula: f_{%o,Q}(P) * f_{%o,pi(Q)}(P) * l_{(%o)Q, (%o)pi(Q)}(P)\n", R[2][1], R[2][2], R[2][1], R[2][2];
printf "we have (%o) + (%o)*qx = %o mod rx\n", R[2][1], R[2][2], ((R[2][1] + qx*R[2][2]) mod rx);
printf "because (%o)P + (%o)pi(P) == 0 for all P of order r, the line l is a vertical and can be removed.\n", R[2][1], R[2][2];

exponent := (10609*x^18 - 56856*x^17 + 81429*x^16 + 94486*x^15 - 346260*x^14 + 153777*x^13 + 375030*x^12 - 484569*x^11 + 121200*x^10 + 378524*x^9 - 520992*x^8 + 11613*x^7 + 310419*x^6 - 147012*x^5 + 143748*x^4 + 245041*x^3 + 33708*x^2 + 63585*x + 50461)/27;

assert exponent eq (Evaluate(CyclotomicPolynomial(6), qx) div rx);
assert (tx^2 + 3*yx^2) eq 4*qx;
assert (((qx+1-tx) mod rx) eq 0);
cx := (qx+1-tx) div rx; // cofactor
assert ((lambx^2+ lambx + 1) mod rx) eq 0;
assert ((omegx^2+ omegx + 1) mod qx) eq 0;
cx eq (103*x^6 - 173*x^5 - 96*x^4 + 293*x^3 + 21*x^2 + 52*x + 172)/3;
// Horner rule:
cx eq ((((((103*x - 173)*x - 96)*x + 293)*x + 21)*x + 52)*x + 172)/3;

// computing the order of the curve for G2
printf "tx = %o\nyx = %o\norder of the twist:\n", tx,yx;
if (qx+1-(3*yx+tx)/2) mod rx eq 0 then
    order_tw6 := (qx+1-(3*yx+tx)/2);
    tx_tw6 := (3*yx+tx)/2;
    printf "(qx+1-(3*yx+tx)/2) = %o\n = rx * (%o)\ntx_tw6 = (3*yx+tx)/2 = %o\n", order_tw6, order_tw6 div rx, tx_tw6;
    // computing yx_tw6:
    // tx_tw6^2 - 4*qx = -3*yx_tw6^2
    // tx_tw6^2 - 4*qx = ((3*yx+tx)/2)^2 - 4*(tx^2+3*yx^2)/4
    //                 = (9*yx^2 + tx^2 + 6*yx*tx)/4 - (4*tx^2 + 12*yx^2)/4
    //                 = (-3*yx^2 -3*tx^2 + 6*yx*tx)/4
    //                 = -3*(yx^2+tx^2-2*yx*tx)/4
    //                 = -3*((tx-yx)/2)^2
    yx_tw6 := (tx-yx)/2;
    assert (tx_tw6^2 + 3*yx_tw6^2) eq 4*qx;
    printf "yx_tw6 = (tx-yx)/2 = %o\n", yx_tw6;
elif (qx+1-(-3*yx+tx)/2) mod rx eq 0 then
    order_tw6 := (qx+1-(-3*yx+tx)/2);
    tx_tw6 := (-3*yx+tx)/2;
    printf "(qx+1-(-3*yx+tx)/2) = %o\n = rx * (%o)\ntx_tw6 = (-3*yx+tx)/2 = %o\n", order_tw6, order_tw6 div rx, tx_tw6;
    // computing yx_tw6:
    // tx_tw6^2 - 4*qx = -3*yx_tw6^2
    // tx_tw6^2 - 4*qx = ((-3*yx+tx)/2)^2 - 4*(tx^2+3*yx^2)/4
    //                 = (9*yx^2 + tx^2 - 6*yx*tx)/4 - (4*tx^2 + 12*yx^2)/4
    //                 = (-3*yx^2 -3*tx^2 - 6*yx*tx)/4
    //                 = -3*(yx^2+tx^2+2*yx*tx)/4
    //                 = -3*((tx+yx)/2)^2
    yx_tw6 := (tx+yx)/2;
    assert (tx_tw6^2 + 3*yx_tw6^2) eq 4*qx;
    printf "yx_tw6 = (tx+yx)/2 = %o\n", yx_tw6;
else
    print("error computing order of sextic twist");
end if;

tx_tw6 eq (20*x^6 - 37*x^5 - 9*x^4 + 49*x^3 + 17*x + 29)/3;
order_tw6 eq (103*x^12 - 379*x^11 + 250*x^10 + 691*x^9 - 911*x^8 - 79*x^7 + 563*x^6 - 529*x^5 + 301*x^4 + 616*x^3 + 73*x^2 + 203*x + 151)/9;
cx_tw6 := order_tw6 div rx;
cx_tw6 eq (103*x^6 - 173*x^5 - 96*x^4 + 293*x^3 + 21*x^2 + 52*x + 151)/3;
// Horner rule:
cx_tw6 eq ((((((103*x - 173)*x - 96)*x + 293)*x + 21)*x + 52)*x + 151)/3;

// Fast clearing co-factor for G1 and G2:
// computing R = c *P  for P in E(Fq) so that R is in G1 (R has order r)
//           R'= c'*P' for P' in E'(Fq) so that R' is in G2 (R' has order r)
// Fast subgroup check for G1 and G2
// check that r*P = O for R in E(Fq)
// check that r*P'= O for R in E'(Fq)

D := 3;
k := 6;
// 1. computing Lx such that Lx^2+Lx+1 = 0 mod qx+1-tx = order
// Lx corresponds to a 3rd root of unity so first we compute sqrt(-3) mod order.
// order = qx + 1 -tx = ((tx-2)^2 + 3*yx^2)/4
// sqrt(-3) = (tx-2)/yx mod order
Gcd(order, yx) eq 1; // yes
g,u,v := Xgcd(order, yx);
g eq u*order + v*yx; // now 1/yx = v mod order
sqrt_3_mod_order := ((tx-2)*v) mod order;
Lx := (-1 + sqrt_3_mod_order)/2;
assert ((Lx^2 + Lx + 1) mod order) eq 0;
Lx eq (2701587*x^11 - 14399455*x^10 + 29479800*x^9 - 16723161*x^8 - 29435567*x^7 + 73269543*x^6 - 57047220*x^5 - 28256269*x^4 + 79789227*x^3 - 8898582*x^2 + 14602051*x + 31851294)/3751260;
Lxc := Lx mod cx;
Lxc eq (-385941*x^5 + 1285183*x^4 - 1641034*x^3 - 121163*x^2 + 1392389*x - 1692082)/1250420;
M := Matrix(QQx, 2, 2, [cx, 0, -Lxc, 1]);
//print M;
reducedM := LLL(M);
//print reducedM;
reducedM *:= 103;
print reducedM;
//[          -7*x^2 - 89*x - 130  103*x^3 - 90*x^2 - 129*x + 6]
//[103*x^3 - 83*x^2 - 40*x + 136            7*x^2 + 89*x + 130]
a0 := reducedM[1][1]; a1 := reducedM[1][2];
b0 := reducedM[2][1]; b1 := reducedM[2][2];
printf "checking that a0+a1*Lx = 0 mod cx ";
((a0 + a1*Lx) mod cx) eq 0;
printf "checking that ((a0+a1*Lx) div cx) is coprime to rx ";
Gcd((a0 + a1*Lx) div cx, rx) eq 1;
printf "checking that b0+b1*Lx = 0 mod cx ";
((b0 + b1*Lx) mod cx) eq 0;
printf "checking that ((b0+b1*Lx) div cx) is coprime to rx ";
Gcd((b0 + b1*Lx) div cx, rx) eq 1;

// check that is works
q := ZZ ! Evaluate(qx, u0);
r := ZZ ! Evaluate(rx, u0);
c := ZZ ! Evaluate(cx, u0);
Fq := FiniteField(q);
E := EllipticCurve([Fq ! 0, Fq ! -1]);
w := Fq ! Evaluate(omegx, u0);
assert w^2+w+1 eq 0;
ord := ZZ ! Evaluate(order, u0);
lamb := Evaluate(Lx, u0); // but there is a denominator

for i:= 1 to 30 do
    P := Random(E);
    //phi_P := E ! [w*P[1], P[2]];
    xP := u0*P;
    x2P := u0*xP;
    x3P := u0*x2P;
    Q := (7*x2P +89*xP + 130*P);
    phi_Q := E ! [w*Q[1], Q[2]];
    R := (103*x3P -83*x2P -40*xP + 136*P) + phi_Q;
    assert r*R eq (E ! 0);
end for;
// it works

// checking membership in G1, that is, r*P = O
Lxr := Lx mod rx;
Lxr eq x^5 - 3*x^4 + 3*x^3 - x + 1;

M := Matrix(QQx, 2, 2, [rx, 0, -Lxr, 1]);
//print M;
reducedM := LLL(M);
print reducedM;
//[        x + 1 x^3 - x^2 + 1]
//[x^3 - x^2 - x        -x - 1]
a0 := reducedM[1][1]; a1 := reducedM[1][2];
b0 := reducedM[2][1]; b1 := reducedM[2][2];
printf "checking that a0+a1*Lx = 0 mod rx ";
((a0 + a1*Lx) mod rx) eq 0;
printf "checking that ((a0+a1*Lx) div rx) is coprime to cx ";
Gcd((a0 + a1*Lx) div rx, cx) eq 1;
printf "checking that b0+b1*Lx = 0 mod rx ";
((b0 + b1*Lx) mod rx) eq 0;
printf "checking that ((b0+b1*Lx) div rx) is coprime to cx ";
Gcd((b0 + b1*Lx) div rx, cx) eq 1;

// formula 1 :       (x+1)*P + phi((x^3-x^2+1)*P)
// formula 2 : (x^3-x^2-x)*P - phi((x+1)*P)
// check that is works

for i:= 1 to 30 do
    P := Random(E);
    xP := u0*P;
    x2P := u0*xP;
    x3P := u0*x2P;
    Q := (x3P -x2P + P);
    phi_Q := E ! [w*Q[1], Q[2]];
    R := xP+P + phi_Q;
    assert R ne (E ! 0);
    rP := c*P;
    xrP := u0*rP;
    x2rP := u0*xrP;
    x3rP := u0*x2rP;
    rQ := (x3rP -x2rP + rP);
    phi_rQ := E ! [w*rQ[1], rQ[2]];
    rR := xrP+rP + phi_rQ;
    assert rR eq (E ! 0);
end for;
// it works

// G2
order_tw6 := qx + 1 - tx_tw6;
Gcd(order_tw6, yx_tw6) eq 1; // yes
g,u,v := Xgcd(order_tw6, yx_tw6);
g eq (u*order_tw6 + v*yx_tw6);
sqrt_3_mod_order := ((tx_tw6-2)*v) mod order_tw6;
Lx_tw6 := (-1 + sqrt_3_mod_order)/2;
assert ((Lx_tw6^2 + Lx_tw6 + 1) mod order_tw6) eq 0;
d0 := Lcm([Denominator(ai) : ai in Eltseq(Lx_tw6)]);
//printf "Lx_tw6 = (%o)/%o\n", d0*Lx_tw6, d0;
//Lx_tw6 eq (9895004*x^11 - 45170643*x^10 + 63901250*x^9 + 8384047*x^8 - 90117699*x^7 + 75129319*x^6 - 24322762*x^5 - 34127088*x^4 + 70158536*x^3 + 5265568*x^2 + 9239205*x + 20627350)/889857;
// for compatibility with the choice of omega in Fq, I need to consider the other cube root of unity -Lx_tw6-1
Lx_tw6 := -Lx_tw6-1;
printf "Lx_tw6 = (%o)/%o\n", d0*Lx_tw6, d0;
Lxc_tw6 := Lx_tw6 mod cx_tw6;
d1 := Lcm([Denominator(ai) : ai in Eltseq(Lxc_tw6)]);
printf "Lxc_tw6 = (%o)/%o\n", d1*Lxc_tw6, d1;
//Lxc_tw6 eq (1413572*x^5 - 3625805*x^4 + 1877140*x^3 + 2124857*x^2 - 1372633*x + 1972159)/296619;
Lxc_tw6 eq (-1413572*x^5 + 3625805*x^4 - 1877140*x^3 - 2124857*x^2 + 1372633*x - 2268778)/296619;

M := Matrix(QQx, 2, 2, [cx_tw6, 0, -Lxc_tw6, 1]);
//print M;
reducedM := LLL(M);
//print reducedM;
reducedM *:= 103;
print reducedM;
//[         -7*x^2 + 117*x + 109 103*x^3 - 90*x^2 - 26*x + 136]
//[103*x^3 - 83*x^2 - 143*x + 27           7*x^2 - 117*x - 109]
a0 := reducedM[1][1]; a1 := reducedM[1][2];
b0 := reducedM[2][1]; b1 := reducedM[2][2];
printf "checking that a0+a1*Lx_tw6 = 0 mod cx_tw6 ";
((a0 + a1*Lx_tw6) mod cx_tw6) eq 0;
printf "checking that ((a0+a1*Lx_tw6) div cx_tw6) is coprime to rx ";
Gcd((a0 + a1*Lx_tw6) div cx_tw6, rx) eq 1;
printf "checking that b0+b1*Lx_tw6 = 0 mod cx_tw6 ";
((b0 + b1*Lx_tw6) mod cx_tw6) eq 0;
printf "checking that ((b0+b1*Lx_tw6) div cx_tw6) is coprime to rx ";
Gcd((b0 + b1*Lx_tw6) div cx_tw6, rx) eq 1;
// check that is works

E1 := EllipticCurve([Fq ! 0, Fq ! 4]);
c1 := ZZ ! Evaluate(cx_tw6, u0);
assert Order(E1) eq c1*r;

for i:= 1 to 30 do
    P := Random(E1);
    xP := u0*P;
    x2P := u0*xP;
    x3P := u0*x2P;
    Q := (7*x2P -117*xP - 109*P);
    phi_Q := E1 ! [w*Q[1], Q[2]];
    R := (103*x3P -83*x2P -143*xP + 27*P) + phi_Q;
    assert r*R eq (E1 ! 0);
    Q := (103*x3P - 90*x2P - 26*xP + 136*P);
    phi_Q := E1 ! [w*Q[1], Q[2]];
    R := (-7*x2P + 117*xP + 109*P) + phi_Q;
    assert r*R eq (E1 ! 0);
end for;
// it works

// now subgroup membership testing
// we should have the same formulas as for G1
//Lxr_tw6 := Lx_tw6 mod rx;
Lxr_tw6 := x^5 - 3*x^4 + 3*x^3 - x + 1;
//Lxr_tw6 eq -x^5 + 3*x^4 - 3*x^3 + x - 2;
assert (Lxr_tw6^2+Lxr_tw6+1) mod rx eq 0;
assert (Lx_tw6^2+Lx_tw6+1) mod (cx_tw6*rx) eq 0;

M := Matrix(QQx, 2, 2, [rx, 0, -Lx_tw6, 1]);
//print M;
reducedM := LLL(M);
print reducedM;
//[       -x - 1 x^3 - x^2 - x]
//[x^3 - x^2 + 1         x + 1]
// or
//[        x + 1 x^3 - x^2 + 1]
//[x^3 - x^2 - x        -x - 1]
a0 := reducedM[1][1]; a1 := reducedM[1][2];
b0 := reducedM[2][1]; b1 := reducedM[2][2];
printf "checking that a0+a1*Lx_tw6 = 0 mod rx ";
((a0 + a1*Lx_tw6) mod rx) eq 0;
printf "checking that ((a0+a1*Lx_tw6) div rx) is coprime to cx_tw6 ";
Gcd((a0 + a1*Lx_tw6) div rx, cx_tw6) eq 1;
printf "checking that b0+b1*Lx_tw6 = 0 mod rx ";
((b0 + b1*Lx_tw6) mod rx) eq 0;
printf "checking that ((b0+b1*Lx_tw6) div rx) is coprime to cx_tw6 ";
Gcd((b0 + b1*Lx_tw6) div rx, cx_tw6) eq 1;

// check that is works

for i:= 1 to 30 do
    P := Random(E1);
    xP := u0*P;
    x2P := u0*xP;
    x3P := u0*x2P;
    Q := (x3P -x2P + P);
    phi_Q := E1 ! [(-w-1)*Q[1], Q[2]];
    R := xP+P + phi_Q;
    
    assert R ne (E1 ! 0);
    rP := c1*P;
    xrP := u0*rP;
    x2rP := u0*xrP;
    x3rP := u0*x2rP;

    rQ := (x3rP -x2rP + rP);
    phi_rQ := E1 ! [(-w-1)*rQ[1], rQ[2]];
    rR := xrP+rP + phi_rQ;
    assert rR eq (E1 ! 0);

    Q := (x3P -x2P - xP);
    phi_Q := E1 ! [w*Q[1], Q[2]];
    R := -xP-P + phi_Q;
    assert R ne (E1 ! 0);
    rQ := (x3rP -x2rP - xrP);
    phi_rQ := E1 ! [w*rQ[1], rQ[2]];
    rR := -xrP-rP + phi_rQ;
    assert rR eq (E1 ! 0);

end for;
// it works

// Computing a multiple of the exponent Phi_k(q)/r so that in basis 1,q,q^2, ..., q^(EulerPhi(k)-1) it has short coefficients
// EulerPhi(6) = 2, we consider the basis (1,qx), in other words deg(exponent)=18 and deg(qx)=12
// Generic code:
/*
k:= 6;
dx := exponent;
i := Degree(qx)-1;
j := Floor(Degree(dx)/Degree(qx));
j := EulerPhi(k);
Row1 := Matrix(QQx, 1, j, [dx] cat [0: ei in [1..j-1]]);
Col1 := Matrix(QQx, j-1, 1, [(-qx^ej) mod dx : ej in [1 .. j-1]]);
N := HorizontalJoin(Col1, IdentityMatrix(QQx,j-1));
M := VerticalJoin(Row1, N);
*/
M := Matrix(QQx, 2, 2, [exponent, 0, -qx, 1]);
R := LLL(M);
print R;

for rr in Rows(R) do
    print [Degree(ri) : ri in Eltseq(rr)];
end for;

/*
[-x^7 + 70/103*x^6 + 269/103*x^5 - 197/103*x^4 - 314/103*x^3 - 73/103*x^2 - 263/103*x - 220/103   x^9 - 276/103*x^8 + 77/103*x^7 + 492/103*x^6 - 445/103*x^5 - 65/103*x^4 + 452/103*x^3 - 181/103*x^2 + 34/103*x + 229/103]
[x^9 - 276/103*x^8 - 26/103*x^7 + 562/103*x^6 - 176/103*x^5 - 262/103*x^4 + 138/103*x^3 - 254/103*x^2 - 229/103*x + 9/103   x^7 - 70/103*x^6 - 269/103*x^5 + 197/103*x^4 + 314/103*x^3 + 73/103*x^2 + 263/103*x + 220/103]
*/
R0 := R[1][1];
R1 := R[1][2];

(R0 + R1*qx) mod exponent;

R2 := R[2][1];
R3 := R[2][2];
(R2 + R3*qx) mod exponent;

R0 := (-103*x^7 + 70*x^6 + 269*x^5 - 197*x^4 - 314*x^3 - 73*x^2 - 263*x - 220);
R1 := (103*x^9 - 276*x^8 + 77*x^7 + 492*x^6 - 445*x^5 - 65*x^4 + 452*x^3 - 181*x^2 + 34*x + 229);

(R0 + R1*qx) mod exponent;

cofact, rem := Quotrem((R0 + R1*qx), exponent);
rem eq 0;

cofact eq 3*(x^3 - x^2 + 1);

(R0 + R1*qx) eq 3*(x^3 - x^2 + 1)*exponent;

printf "R0 = %o\n", R0;
printf "R1 = %o\n", R1;


// FASTER subgroup-chek for G1 and G2.
// https://eprint.iacr.org/2019/814

// Final exponentiation for BLS12 curve (valid for any curve: 377, 381...)
// where r_BLS12 is Phi_{12} and q_BLS12 is our "rx"
ee := Evaluate(CyclotomicPolynomial(12), rx) div CyclotomicPolynomial(12);
b3, rem3 := Quotrem(ee, rx^3);
b2, rem2 := Quotrem(rem3, rx^2);
b1, b0 := Quotrem(rem2, rx);
ee eq (b0 + b1*rx + b2*rx^2 + b3*rx^3);
3*ee eq ((x^2 - 2*x + 1)*rx^3 + (x^3 - 2*x^2 + x)*rx^2 + (x^4 - 2*x^3 + 2*x - 1)*rx + (x^5 - 2*x^4 + 2*x^2 - x + 3));
// Fuentes et al faster final exponentiation
printf "BLS12 final exponentiation, Fuentes-et-al method\n";
M := Matrix(QQx, 4,4, [ee, 0,0,0, rx,1,0,0, rx^2,0,1,0, rx^3,0,0,1]);
LLL(M);
// does not give smaller coefficients


// check GLV method, G1 and G2
// for a point P of order r, we have phi(P) = [lambda]P, but there are two choices of phi and two choices of Lambda mod r.
omega1 := Fq ! Evaluate(omegx, u0);
omega2 := -omega1-1;
assert omega1^2 + omega1 + 1 eq 0;
assert omega2^2 + omega2 + 1 eq 0;

Fr := FiniteField(r);
lambda1 := ZZ ! (Fr ! Evaluate(lambx, u0));
lambda2 := ZZ ! (Fr ! -lambda1-1);
assert (Fr ! (lambda1^2 + lambda1 + 1)) eq 0;
assert (Fr ! (lambda2^2 + lambda2 + 1)) eq 0;

function phi_1(P)
    E := Curve(P);
    //omega1 := 0x531DC16C6ECD27AA846C61024E4CCA6C1F31E53BD9603C2D17BE416C5E4426EE4A737F73B6F952AB5E57926FA701848E0A235A0A398300C65759FC45183151F2F082D4DCB5E37CB6290012D96F8819C547BA8A4000002F962140000000002A;
    return E ! [omega1 * P[1], P[2]];
end function;

function phi_2(P)
    E := Curve(P);
    //omega2 := 0xCFCA638F1500E327035CDF02ACB2744D06E68545F7E64C256AB7AE14297A1A823132B971CDEFC65870636CB60D217FF87FA59308C07A8FAB8579E02ED3CDDCA5B093ED79B1C57B5FE3F89C11811C1E214983DE300000535E7BC00000000060;
    return E ! [omega2 * P[1], P[2]];
end function;

printf "omega1  = %o\n        = %h\n", omega1, ZZ ! omega1;
printf "omega2  = %o\n        = %h\n", omega2, ZZ ! omega2;
printf "lambda1 = %o\n        = %h\n", lambda1, ZZ ! lambda1;
printf "lambda2 = %o\n        = %h\n", lambda2, ZZ ! lambda2;
printf "For G1, on E: %o\n", E;
P := c*Random(E);
if phi_1(P) eq lambda1*P and phi_2(P) eq lambda2*P then
    printf "phi_1(P) == [lambda1]*P\n";
    printf "phi_2(P) == [lambda2]*P\n";
elif phi_1(P) eq lambda2*P and phi_2(P) eq lambda1*P then
    printf "phi_1(P) == [lambda2]*P\n";
    printf "phi_2(P) == [lambda1]*P\n";
end if;
// E1 of order r*c1 is a 6-th twist

printf "For G2, on E': %o\n", E1;
Q := c1*Random(E1);
if phi_1(Q) eq lambda1*Q and phi_2(Q) eq lambda2*Q then
    printf "phi_1(Q) == [lambda1]*Q\n";
    printf "phi_2(Q) == [lambda2]*Q\n";
elif phi_1(Q) eq lambda2*Q and phi_2(Q) eq lambda1*Q then
    printf "phi_1(Q) == [lambda2]*Q\n";
    printf "phi_2(Q) == [lambda1]*Q\n";
end if;

